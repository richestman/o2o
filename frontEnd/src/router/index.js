import Vue from "vue";
import Router from "vue-router";
// 路由懒加载
//加载pc端路由
const getView = name => () => import(`../web/${name}.vue`);
//加载手机端路由
const getMobile = name => () => import(`../mobile/${name}.vue`);

Vue.use(Router);

export default new Router({
  routes: [
    //pc端
    {
      path: "/Windex",
      name: "Windex",
      component: getView("Windex"),
      title: "首页"
    },
    {
      path: "/Information",
      name: "Information",
      component: getView("Information"),
      title: "资讯列表"
    },
    {
      path: "/Goods",
      name: "Goods",
      component: getView("Goods"),
      title: "服务列表"
    },
    {
      path: "/GoodsDetails",
      name: "GoodsDetails",
      component: getView("GoodsDetails"),
      title: "服务详情"
    },
    {
      path: "/About",
      name: "About",
      component: getView("About"),
      title: "联系我们"
    },
    {
      path: "/Introduction",
      name: "Introduction",
      component: getView("Introduction"),
      title: "平台介绍"
    },
    {
      path: "/Login",
      name: "Login",
      component: getView("Login"),
      title: "登录"
    },
    {
      path: "/Wnews",
      name: "Wnews",
      component: getView("Wnews"),
      title: "新闻详情"
    },
    //手机端
    { path: "/Tabbar", name: "Tabbar", component: getMobile("tabbar/Tabbar") },
    {
      path: "/Mine",
      name: "Mine",
      component: getMobile("tabbar/me/Mine"),
      meta: { title: "个人中心" }
    },
    {
      path: "/Index",
      name: "Index",
      component: getMobile("tabbar/index/Index"),
      meta: { title: "首页" }
    },
    {
      path: "/Qrcode",
      name: "Qrcode",
      component: getMobile("tabbar/index/Qrcode"),
      meta: { title: "二维码" }
    },
    {
      path: "/GoodsMobile",
      name: "GoodsMobile",
      component: getMobile("tabbar/goods/Goods"),
      meta: { title: "服务列表" }
    },
    {
      path: "/Goodsinfo",
      name: "Goodsinfo",
      component: getMobile("tabbar/goods/Goodsinfo"),
      meta: { title: "服务详情" }
    },
    {
      path: "/News",
      name: "News",
      component: getMobile("tabbar/new/Newlist"),
      meta: { title: "新闻中心" }
    },
    {
      path: "/Newsinfo",
      name: "Newsinfo",
      component: getMobile("tabbar/new/Newsinfo"),
      meta: { title: "新闻详情" }
    },
    {
      path: "/LoginMobile",
      name: "LoginMobile",
      component: getMobile("tabbar/login/Login"),
      meta: { title: "登录" }
    },
    {
      path: "/MAbout",
      name: "MAbout",
      component: getMobile("MAbout"),
      meta: { title: "关于我们" }
    }
  ]
});
