import axios from 'axios'
export default {
    apiGet(url, params) {
      var that=this
      return new Promise((resolve, reject) => {
        axios.get('http://106.54.163.65/api.php/'+url, {params}).then((response) => {
          resolve(response)
        }, (response) => {
          reject(response)
          _g.closeGlobalLoading()
          that.$message({
            message: '请求超时，请检查网络',
            type: 'warning'
          })
        }).catch(error => {
          console.log('shibai ',error)
        })
      })
    },
    apiPost(url, data) {
      var that = this
      // headers: {'Authorization': token }
      return new Promise((resolve, reject) => {
        axios.post('http://106.54.163.65/api.php/'+url, data,).then((response) => {
          resolve(response.data)
        }).catch((response) => {
          console.log('f', response)
          resolve(response)
          that.$message({
            message: '请求超时，请检查网络',
            type: 'warning'
          })
        })
      })
    },
    apiDownload(url,param){
      return new Promise((resolve, reject) => {
        axios.post(url, param, {

          'responseType': 'blob'  //设置响应的数据类型为一个包含二进制数据的 Blob 对象，必须设置！！！
        }).then(function (response) {
          const blob = new Blob([response.data]);
          const fileName = Math.floor(Math.random()*1000000000)+'.xlsx';
          const linkNode = document.createElement('a');

          linkNode.download = fileName; //a标签的download属性规定下载文件的名称
          linkNode.style.display = 'none';
          linkNode.href = URL.createObjectURL(blob); //生成一个Blob URL
          document.body.appendChild(linkNode);
          linkNode.click();  //模拟在按钮上的一次鼠标单击

          URL.revokeObjectURL(linkNode.href); // 释放URL 对象
          document.body.removeChild(linkNode);
          resolve(response.data)
        }).catch(function (error) {
          console.log(error);
          resolve(error)
        });
      });


    },
    apiDelete(url, id) {
      return new Promise((resolve, reject) => {
        axios.delete(url + id).then((response) => {
          resolve(response.data)
        }, (response) => {
          reject(response)
          _g.closeGlobalLoading()
          bus.$message({
            message: '请求超时，请检查网络',
            type: 'warning'
          })
        })
      })
    },
    apiPut(url, id, obj) {
      return new Promise((resolve, reject) => {
        axios.put(url + id, obj).then((response) => {
          resolve(response.data)
        }, (response) => {
          _g.closeGlobalLoading()
          bus.$message({
            message: '请求超时，请检查网络',
            type: 'warning'
          })
          reject(response)
        })
      })
    }
  }

