<?php
namespace app\admin\controller;
use think\Controller;
use think\Db;
use think\Input;

class Companyrole extends Common
{
    public function _initialize(){
        parent::_initialize();
    }
    public function index()
    {
        if (!session('aid')){
            $this->redirect('login/login');
        }
        //读取公司列表
        $company = db("company")->field("c_id,c_name")->select();
        $this->assign("company",$company);
        //获取当前选中的公司id
        $cid = input("cid",0);
        if(count($company)>0&&$cid==0){
            $cid = $company[0]['c_id'];
        }


        $this->assign("companyid",$cid);
        //$admin_group=db('company')->where(array('c_id'=>$cid))->find();
        //读取权限
        $user = db("c_admin")->where("a_companyid",$cid)->where("a_isadmin","1")->find();



        $admin_group = db("c_auth_group")->where("g_id",$user['a_gid'])->find();

        $authRule = db('auth_rule');
        $data = $authRule->field('r_id,r_name,r_title')->where(array('r_pid'=>0,'r_authopen'=>0))->order('r_sort')->select();
        foreach ($data as $k=>$v){
            $data[$k]['sub'] = $authRule->field('r_id,r_name,r_title')->where(array('r_pid'=>$v['r_id'],'r_authopen'=>0))->order('r_sort')->select();
            foreach ($data[$k]['sub'] as $kk=>$vv){
                $data[$k]['sub'][$kk]['sub'] = $authRule->field('r_id,r_name,r_title')->where(array('r_pid'=>$vv['r_id'],'r_authopen'=>0))->order('r_sort')->select();
                foreach ($data[$k]['sub'][$kk]['sub'] as $kkk=>$vvv){
                    $data[$k]['sub'][$kk]['sub'][$kkk]['sub'] =$authRule->field('r_id,r_name,r_title')->where(array('r_pid'=>$vvv['r_id'],'r_authopen'=>0))->order('r_sort')->select();
                }
            }
        }
        $this->assign("user",$user);
        $this->assign('admin_group',$admin_group);	// 当前用户权限列表
        $this->assign('datab',$data);	// 顶级
        return $this->fetch();
    }
    //分组配置规则

    public function updateRole(){
        $authGroup = db('auth_group');
        if(empty($_POST['new_rules'])){
            $this->error('请选择权限！',0,0);
        }
        $new_rules = $_POST['new_rules'];
        $imp_rules = implode(',', $new_rules).',';
        $uid = input("uid");
        $cid = input('id');  //角色表id
        $companyid = input("company");  //公司id
        $sldata=array(
            'g_rules'=>$imp_rules
        );
        //修改公司总管理员拥有的功能木块
        //$r = db("company")->where("c_id",$cid)->update($sldata);
        $r = db("c_auth_group")->where('g_id',$cid)->update($sldata);
        if($r){
            $result['info'] = '权限配置成功!';
            $result['url'] = url('index',array('cid'=>$companyid));
            $result['status'] = 1;
            return $result;
        }else{
            $this->error('配置没有改变，无需保存',url('index',array('cid'=>$companyid)),0);
        }
    }
    
}
