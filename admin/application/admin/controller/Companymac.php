<?php
/**
 * Created by PhpStorm.
 * User: ZHANG
 * Date: 2018/11/28
 * Time: 9:24
 */

namespace app\admin\controller;
use think\Controller;
use app\admin\model\Companymacs;
use think\Model;
class Companymac extends Common
{
    //显示电脑mac页面
    public function companymaclist()
    {
        //实例化类
        $aa = new Companymacs();
        //使用方法
        $c_id=input('c_id');
        $c_compute_number=input('c_compute_number');
        $ab = $aa->companymaclist($c_id);

        $this->assign('c_id',$c_id);
        $this->assign('c_compute_number',$c_compute_number);
        $this->assign('companymaclist',$ab);
        return $this->fetch();
    }
    //显示公司电脑添加页面
    public function companymac_add()
    {
            $c_id=input('c_id');
            //实例化类
            $aa = new Companymacs();
            //使用方法
            $a = $aa->companymac_add($c_id);
            $this->assign('alist',$a);
            return $this->fetch();

    }
    //显示公司电脑修改页面
    public function companymac_edit()
    {
        $id=input('m_id');
        //实例化类;
        $aa = new Companymacs();
        //使用方法
        $info = $aa->companymac_edit($id);
        $this->assign('catinfo',$info['dblist']);
        return $this->fetch();
    }
    //公司页面电脑添加方法
    public function do_addcommac()
    {
        $data = input('post.');
        $data['m_createtime']=time();
        //实例化类
        $aa = new Companymacs();
        //使用方法
        $info = $aa->do_addcommac($data);
        return json_encode($info);
    }
    //公司页面电脑修改方法
    public function do_updcommac()
    {
        $data=input('post.');
        //实例化类
        $aa = new Companymacs();
        //使用方法
        $info = $aa->do_updcommac($data);
        return json_encode($info);
    }
    //公司电脑页面删除
    public function comapnymac_del()
    {
        $id=input('m_id');
        //实例化类
        $aa = new Companymacs();
        //使用方法
        $info = $aa->comapnymac_del($id);
        return json_encode($info);
    }

}

