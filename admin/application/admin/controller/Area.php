<?php
namespace app\admin\controller;
use think\Controller;
use think\Input;
use app\admin\model\Areas;
class Area extends Common
{
     //显示总页面
    public function index()
    {
        $data = input('select');

        //实例化类
       $ar = new Areas();
       //使用方法
       $area =   $ar->index( $data);
     $this->assign('alist',$area['area']);
     //映射公司列表内容
     $this->assign('clist',$area['clist']);
     $this->assign('data',$data);

        return $this->fetch();
    }
    //  接收id
    public function area_add(){
        $a_id=input('a_id');
        $this->assign('a_id',$a_id);
        //实例化类
       $add = new Areas();
     $area_add = $add->area_add($a_id);
       $this->assign('alist',$area_add['alist']);
       $this->assign('clist',$area_add['clist']);
        $this->assign('list',$area_add['list']);

        return $this->fetch();
    }
    //把数据添加到数据库
    public function area_do_add(){
        $data = input('post.');
        //实例化类
        $do_add = new Areas();
        $area_do = $do_add->area_do_add($data);


    }
    //映射修改的html页面
    public function area_edit(){
        $a_id=input('a_id');
        $this->assign('a_id',$a_id);
        //实例化类
        $add = new Areas();
        $area_add = $add->area_edit($a_id);
//        echo $area_add;exit;

        $this->assign('alist',$area_add['alist']);
        $this->assign('list',$area_add['list']);
        $this->assign('clist',$area_add['clist']);
        $this->assign('list2',$area_add['list2']);
        return $this->fetch();
    }
    //修改数据导数据库
    public function  area_do_edit(){
        $data =input('post.');
        //实例化类
        $add = new Areas();
        $area_add = $add->area_do_edit($data);
        return json_encode($area_add);
    }
    public function area_del(){
        $a_id = input('a_id');
//        echo $a_id;exit;
        $flag =input('flag');

        $s = [
            'a_id'=>$a_id,
            'flag'=>$flag,

        ];
        //实例化类
        $add = new Areas();
        $area_add = $add->area_del($s);
        return json_encode($area_add);
    }

//映射顶级站点页面
public function area_top(){
        //实例化类
    $add = new Areas();
    $area_add = $add->area_top();
    $this->assign('alist',$area_add['alist']);
    $this->assign('clist',$area_add['clist']);
        return $this->fetch();

}
//顶级站点添加数据到数据库
public function area_do_top(){
   $data = input('post.');
    //实例化类
    $add = new Areas();
    $area_add = $add->area_do_top($data);

 }
 //通过id显示对应的公司
    public function company(){
        $id = input('c_id');
        //实例化类
        $comp = new Areas();
        $info = $comp->company($id);
        return json_encode($info) ;
    }
}
