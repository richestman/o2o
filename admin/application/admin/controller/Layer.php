<?php
namespace app\admin\controller;
use think\Controller;
class Layer extends Controller{
    public function index(){
       return $this->fetch();
    }
    public function layer_add(){

        return $this->fetch();
    }
    public function layer_do_add(){
        $area = db('area')->where('a_pid=0')->order('a_id desc')->select();
        foreach ($area as $key =>&$v){
            $v['secondlist']=db('area')->where('a_pid='.$v['a_id'])->select();
            foreach ($v['secondlist'] as $k=>&$t)
            {
                $t['thirdlist']=db('area')->where('a_pid='.$t['a_id'])->select();
                foreach ($t['thirdlist'] as $ke=>&$f)
                {
                    $f['forlist']=db('area')->where('a_pid='.$f['a_id'])->select();
                }
            }
        }
        return json_encode($area);
    }
}
?>