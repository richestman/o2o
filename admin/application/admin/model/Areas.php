<?php

namespace app\admin\model;
use think\Model;

class Areas extends Model{
    //遍历总页面的分类 一共几级
    public function index($data){
        //查询顶级
        if (empty($data)){
            $area = db('area')->where('a_pid=0')->order('sort_id desc')->select();
        }else if($data==0){
            $area = db('area')->where('a_pid=0')->order('sort_id desc')->select();
        }
        else{
            $where=[
                'a_company_id'=>['=',$data],
                'a_pid'=>['=',0]
            ];
            $area = db('area')->where($where)->select();
        }

      //遍历2级
      foreach ($area as $key =>&$v){
          $v['secondlist']=db('area')->where('a_pid='.$v['a_id'])->order('sort_id desc')->select();
          //遍历3级
          foreach ($v['secondlist'] as $k=>&$t)
          {
              $t['thirdlist']=db('area')->where('a_pid='.$t['a_id'])->order('sort_id desc')->select();
              //遍历4级
              foreach ($t['thirdlist'] as $k=>&$f)
              {
                  $f['for']=db('area')->where('a_pid='.$f['a_id'])->order('sort_id desc')->select();
              }
          }

      }
        $clist = db('company')
        ->field('c_name,c_id')
        ->order('c_id desc')
        ->select();
      $arr = [
          'area'=>$area,
          'clist'=>$clist

      ];

      return $arr;

    }
    //添加传过来的id 进行处理
    public function area_add($a_id){
        //站点表
      $alist=db('area')
          ->where('a_id='.$a_id)
          ->order('a_id desc')
          ->find();

        //显示数据库对应的id的数据
        $list = db('area')->where('a_id='.$a_id)->find();
       //公司表
        $clist = db('company')
            ->alias('c')
            ->join('__AREA__ a','c_id=a_company_id')
            ->field('c_name,c_id')
            ->where('a_id='.$a_id)
            ->find();

       $ar = [
           'alist'=>$alist,
           'clist'=>$clist,
           'list'=>$list
       ];
      return $ar;
    }
    //添加数据
    public function area_do_add($data){
        $data['a_createtime']=time();
        $a_engname=$data['a_engname'];
        $data['a_engname']=strtoupper($a_engname);


        $info = db('area')->insert($data);
        return $info;
    }
    //接收修改传过来的id进行处理
  public function area_edit($a_id){

      //显示数据库对应的id的数据
      $list2 = db('area')->where('a_id='.$a_id)->find();
      //查看传过来的a_id所对应的a_pid
      $list=db('area')->where('a_id='.$a_id)->value('a_pid');
    //显示所对应的父级
     $alist =db('area')->where('a_id='.$list)->find();
      //公司表
      $clist = db('company')
          ->alias('c')
          ->join('__AREA__ a','c_id=a_company_id')
          ->field('c_name,c_id')
          ->where('a_id='.$a_id)
          ->find();

      $a=[
          'alist'=>$alist,
          'list'=>$list,
          'clist'=>$clist,
          'list2'=>$list2
      ];


      return $a;


  }
  //修改数据
    public function area_do_edit($data){

        $info = db('area')->update($data);
        return $info;
    }
    //删除数据
    public function area_del($s){
        {
            $a_id=$s['a_id'];

            $flag=$s['flag'];
            $children=db('area')->where('a_pid='.$a_id)->select();  //查询其子类的数据$children
            if(!empty($children))  //判断$children是否有值
            {
                if($flag==1)        //$children 没有值时执行如果是第一Ajax 执行
                {
                    $msg=[
                        'code'=>1   //用于进入第二次Ajax
                    ];
                }else{   //如果不是第二次 则执行

                    $childid=db('area')->where('a_pid='.$a_id)->column('a_id');   //返回一个一维数组 为被删除 的子级 的值
                    foreach($childid as $v)                                                       //遍历一维数组
                    {
                        $alist = db('area')->where('a_pid='.$v)->column('a_id');#3
                        foreach ($alist as $j){
                            //查到并删除四级
                            $llast=db('area')->where('a_pid='.$j)->delete();
                        }
                        $childinfo=db('area')->where('a_pid='.$v)->delete();  //删除 被删除级 子级的子级
                    }
                    $info=db('area')->where('a_pid='.$a_id)->delete();  //删除 被删除级 子级
                    $info2=db('area')->where('a_id='.$a_id)->delete();   //删除 被删除级
                    $msg=[
                        'code'=>2,
                        'childid'=>$childid ,//子级的一维数组
                        'alist'=>$alist
                    ];
                    return  $msg;
                }

                return $msg;

            }else{
                $info=db('area')->where('a_id='.$a_id)->delete();  //删除  被删除级
                $msg=[
                    'code'=>2
                ];
                return $msg;
            }

        }
    return  $childid;
    }

    //顶级站点页面
    public function area_top(){
        $alist=db('area')->where('a_pid=0')->select();
        //公司表
        $clist = db('company')->field('c_name,c_id')->select();
        $ar = [
            'alist'=>$alist,
            'clist'=>$clist
        ];
        return $ar;
    }
    //添加顶级插入数据
    public function area_do_top($data){
        $info = db('area')->insert($data);
        return $info;
    }
//通过id找出对应的公司
    public function company($id){

        $area = db('area')
            ->alias('a')
            ->join('__COMPANY__ c','c.c_id=a.a_company_id')
            ->field('c.c_name,a.*')
            ->where('a.a_company_id='.$id)
            ->select();
        return $area;
    }
}