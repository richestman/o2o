<?php
/**
 * Created by PhpStorm.
 * User: ZHANG
 * Date: 2018/11/28
 * Time: 10:38
 */

namespace app\admin\model;

use think\Model;


class Companys extends Model{

    //公司添加页面点鼠标离开公司名称英文缩写时返回的值
    public function company_sx($c_abname){
        $db=db('company')
            ->where("c_abname='"."$c_abname'")
            ->find();
        return $db;
    }
    //公司添加页面鼠标离开用户名时返回的值
    public function company_yhm($a_username){
        $db=db('c_admin')
            ->where("a_username='"."$a_username'")
            ->find();
        return $db;
    }
    //重置密码页面
    public function company_czhis($a_id){
        $data=input('post.');
        $aa = md5($data['a_password']);
        $data=['a_password'=>$aa];
        $db=db('c_admin')->where('a_id='.$a_id)->update($data);
        return $db;
    }
    //公司表
    public function companylist(){
        $db = db('company')
            ->alias('c')
            ->join('c_admin ca','c.c_id=ca.a_companyid')
            ->where("a_isadmin",'1')
            ->order('c_id desc')
            ->select();
        foreach($db as $k=>&$v){
            $v['c_createtime']=date('Y-m-d',$v['c_createtime']);
        }
        return $db;
    }

    //公司表添加方法
    public function do_addcom($data){
    }
    //公司表修改
    public function company_edit($id){
        $where = [
            'c_id'=>$id,
            'a_isadmin'=>'1'
        ];
        $db = db('company')
            ->alias('c')
            ->join('c_admin ca','c.c_id=ca.a_companyid')
            ->where($where)
            ->order('c_id desc')
            ->select();
        return $db;
    }
    //公司表修改方法
    public function do_updcom($data){
        $data['c_is_usermac']=empty($data['c_is_usermac'])?1:$data['c_is_usermac'];
        $data1=[
            'c_id'=>$data['c_id'],
            'c_name'=>$data['c_name'],
            'c_abname'=>$data['c_abname'],
            'c_admin'=>$data['c_admin'],
            'c_address'=>$data['c_address'],
            'c_phone'=>$data['c_phone'],
            'c_is_usermac'=>$data['c_is_usermac'],
            'c_compute_number'=>$data['c_compute_number'],
            'c_createtime'=>strtotime(date('Y-d-m'))
        ];
        $data3=[
            'a_id'=>$data['a_id'],
            'a_is_open'=>$data['a_is_open'],
            'a_username'=>$data['a_username'],
            'a_realname'=>$data['a_realname'],
            'a_addtime'=>strtotime(date('Y-d-m'))
        ];
        $a=db('company')->update($data1);
        $c=db('c_admin')->update($data3);
        return $a.$c;
    }
    //公司表页面删除
    public function comapny_del($id){
        $db=db('company')->where("c_id='$id'")->delete();
        $db2=db('c_auth_group')->where("g_companyid='$id'")->delete();
        $db3=db('c_admin')->where("a_companyid='$id'")->delete();
        $db4=db('company_mac')->where("m_cid='$id'")->delete();
        return $db.$db2.$db3.$db4;
    }

}



