<?php
/**
 * Created by PhpStorm.
 * User: ZHANG
 * Date: 2018/11/28
 * Time: 10:38
 */

namespace app\admin\model;

use think\Model;


class Companymacs extends Model{

    //公司电脑mac显示
    public function companymaclist($c_id){
        $db = db('company_mac')
            ->alias('cm')
            ->join('company c','cm.m_cid=c.c_id')
            ->where("m_cid='$c_id'")
            ->field('cm.*,c.c_name,c.c_id')
            ->order('m_id desc')
            ->select();
        foreach($db as $k=>&$v){
            $v['m_createtime']=date('Y-m-d',$v['m_createtime']);
        }
        return $db;
    }
    //公司电脑mac表添加页面
    public function companymac_add($c_id)
    {
        //公司表
        $db = db('company')->where('c_id='.$c_id)->field('c_name,c_id')->find();
        return $db;
    }
    //公司电脑mac表修改
    public function companymac_edit($id){

        $db=db('company_mac')->where('m_id='.$id)->find();
        $where=[
            'dblist'=>$db
        ];
        return $where;
    }
    //公司电脑表添加方法
    public function do_addcommac($data){
        $db=db('company_mac')->insert($data);
        return $db;
    }
    //公司电脑mac表修改方法
    public function do_updcommac($data){
        $db=db('company_mac')->update($data);
        return $db;
    }
    //公司电脑mac表页面删除
    public function comapnymac_del($id){
        $db=db('company_mac')->where('m_id='.$id)->delete();
        return $db;
    }

}



