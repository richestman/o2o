<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/4/4
 * Time: 下午 11:45
 */

namespace app\home\model;


use think\Model;

class Ordermanagement extends Model
{

    protected $name = 'ordermanagement';
    public function userregistration(){
        return $this->belongsTo('UserRegistration','ur_id');
    }
    public function goods(){
        return $this->belongsTo('Goods','goods_id');
    }
//    }
}