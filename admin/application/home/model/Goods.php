<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/4/5
 * Time: 上午 12:27
 */

namespace app\home\model;


use think\Model;

class Goods extends Model
{
    protected $name = 'goods';
    public function goodscate(){
        return $this->belongsTo('GoodsCate','type_id');
    }
}