<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/4/4
 * Time: 下午 11:27
 */

namespace app\home\model;


use think\Model;

class UserRegistration extends Model
{
    protected $name = 'user_registration';
    public function ordermanagement(){
        return $this->hasMany('Ordermanagement','ur_id');
    }
    public function goods(){
        return $this->hasManyThrough('Goods','Ordermanagement','goods_id');
    }
    public function goodscate(){
        return $this->hasManyThrough('GoodsCate','Goods','type_id');
    }
}