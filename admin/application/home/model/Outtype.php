<?php

namespace app\home\model;
use think\Model;

class Outtype extends Model{
    //遍历总页面的分类 一共几级
    public function index($data){
        //查询顶级
        if (empty($data)){
            $outtype = db('money_outtype')->where(['o_pid'=>['=',0],'o_companyid'=>['=',session('a_companyid')]])->order('o_sort_id desc')->select();
        }else if($data==0){
            $outtype = db('money_outtype')->where(['o_pid'=>['=',0],'o_companyid'=>['=',session('a_companyid')]])->order('o_sort_id desc')->select();
        }
        else{
            $where=[
                'o_pid'=>['=',0]
            ];
            $outtype = db('money_outtype')->where($where)->select();
        }

      //遍历2级
      foreach ($outtype as $key =>&$v){
          $v['secondlist']=db('money_outtype')->where('o_pid='.$v['o_id'])->order('o_sort_id desc')->select();
          //遍历3级
          foreach ($v['secondlist'] as $k=>&$t)
          {
              $t['thirdlist']=db('money_outtype')->where('o_pid='.$t['o_id'])->order('o_sort_id desc')->select();
              //遍历4级
              foreach ($t['thirdlist'] as $k=>&$f)
              {
                  $f['for']=db('money_outtype')->where('o_pid='.$f['o_id'])->order('o_sort_id desc')->select();
              }
          }

      }
        $clist = db('company')
        ->field('c_name,c_id')
        ->order('c_id desc')
        ->select();
      $arr = [
          'outtype'=>$outtype,
          'clist'=>$clist

      ];

      return $arr;

    }
    //添加传过来的id 进行处理
    public function outtype_add($o_id){
        //站点表
      $alist=db('money_outtype')
          ->where('o_id='.$o_id)
          ->order('o_id desc')
          ->find();

        //显示数据库对应的id的数据
        $list = db('money_outtype')->where('o_id='.$o_id)->find();
        $ar = [
           'alist'=>$alist,
           //'clist'=>$clist,
           'list'=>$list
       ];
      return $ar;
    }
    //添加数据
    public function outtype_do_add($data){
        $data['o_createtime']=time();
        $data['o_companyid']=session('a_companyid');
        $info = db('money_outtype')->insert($data);
        return $info;
    }
    //接收修改传过来的id进行处理
  public function outtype_edit($o_id){
      //显示数据库对应的id的数据
      $list2 = db('money_outtype')->where('o_id='.$o_id)->find();
      //查看传过来的o_id所对应的o_pid
      $list=db('money_outtype')->where('o_id='.$o_id)->value('o_pid');
      //显示所对应的父级
      $alist =db('money_outtype')->where('o_id='.$list)->find();
      $a=[
          'alist'=>$alist,
          'list'=>$list,
          'list2'=>$list2
      ];
      return $a;
  }
  //修改数据
    public function outtype_do_edit($data){

        $info = db('money_outtype')->update($data);
        return $info;
    }
    //删除数据
    public function outtype_del($s){
        {
            $o_id=$s['o_id'];

            $flag=$s['flag'];
            $children=db('money_outtype')->where('o_pid='.$o_id)->select();  //查询其子类的数据$children
            if(!empty($children))  //判断$children是否有值
            {
                if($flag==1)        //$children 没有值时执行如果是第一Ajax 执行
                {
                    $msg=[
                        'code'=>1   //用于进入第二次Ajax
                    ];
                }else{   //如果不是第二次 则执行

                    $childid=db('money_outtype')->where('o_pid='.$o_id)->column('o_id');   //返回一个一维数组 为被删除 的子级 的值
                    foreach($childid as $v)                                                       //遍历一维数组
                    {
                        $alist = db('money_outtype')->where('o_pid='.$v)->column('o_id');#3
                        foreach ($alist as $j){
                            //查到并删除四级
                            $llast=db('money_outtype')->where('o_pid='.$j)->delete();
                        }
                        $childinfo=db('money_outtype')->where('o_pid='.$v)->delete();  //删除 被删除级 子级的子级
                    }
                    $info=db('money_outtype')->where('o_pid='.$o_id)->delete();  //删除 被删除级 子级
                    $info2=db('money_outtype')->where('o_id='.$o_id)->delete();   //删除 被删除级
                    $msg=[
                        'code'=>2,
                        'childid'=>$childid ,//子级的一维数组
                        'alist'=>$alist
                    ];
                    return  $msg;
                }

                return $msg;

            }else{
                $info=db('money_outtype')->where('o_id='.$o_id)->delete();  //删除  被删除级
                $msg=[
                    'code'=>2
                ];
                return $msg;
            }

        }
    return  $childid;
    }

    //顶级站点页面
    public function outtype_top(){
        $alist=db('money_outtype')->where('o_pid=0')->select();
        //公司表
        //$clist = db('company')->field('c_name,c_id')->select();
        $ar = [
            'alist'=>$alist,
            //'clist'=>$clist
        ];
        return $ar;
    }
    //添加顶级插入数据
    public function outtype_do_top($data){
        $data['o_companyid']=session('a_companyid');
        $info = db('money_outtype')->insert($data);
        return $info;
    }
//通过id找出对应的公司
    public function company($id){

        $outtype = db('money_outtype')
            ->alias('a')
            ->join('__COMPANY__ c','c.c_id=a.a_company_id')
            ->field('c.c_name,a.*')
            ->where('a.a_company_id='.$id)
            ->select();
        return $outtype;
    }
}