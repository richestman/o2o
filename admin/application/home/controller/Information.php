<?php


namespace app\home\controller;


use think\Controller;
use think\Db;

class Information extends Common
{
    public function _initialize(){
        parent::_initialize();
    }

    /**
     * @return 资讯列表
     */
    public function informlist(){
        $type = input('id','');
        $where = array();

        if ($type){
            $where['ic_id'] = $type;
        }
        $this->assign('type', $type);
        $admininfo2=db('information')
            ->alias('inform')
            ->join('information_classification class','class.id=inform.ic_id')
            ->where($where)
            ->field('inform.sort,inform.id,inform.ic_id,main_title,subtitle,review_type,increase,addtime,class.name,class.str')
            ->order('id','desc')
            ->paginate(10,false,['query' => request()->param()]);
        //查询资讯类别
        $db = db('information_classification')->select();
        $this->assign('typedata', $db);
        $page = $admininfo2->render();
        $this->assign('page',$page);
        $this->assign('data', $admininfo2);
        return $this->fetch('informlist');
    }
    /**
     * @return 资讯列表添加
     */
    public function informlistAdd(){
        $db = db('information_classification')
            ->where('str',1)
            ->select();
//        echo "<meta charset='UTF-8'>";
//        dump($db);die;
        $this->assign('data', $db);
        return $this->fetch('informlistAdd');
    }
    //执行插入数据库
    public function informInsert(){
        $file = request()->file('file');
        if($file){
            $info = $file->move(ROOT_PATH . 'public'. DS . 'uploadinformimg');
            $filename=$info->getSaveName();
            if($filename){
                $auth_group = db('information');
                $data = array(
                    'ic_id' => input('post.typename'),
                    'main_title' => input('post.title'),
                    'subtitle' => input('post.titles'),
                    'image'=>$filename,
                    'content'=>input('post.editorValue'),
                    'review_type'=>input(2),
                    'increase'=>session('a_username'),
                    'addtime' => time(),
                    'sort'=>input('post.sort')
                );

                $auth_group->insert($data);

                $result['info'] = '列表添加成功!';
                $result['url'] = url('informlist');
                $result['status'] = 1;
                return $result;
            }else{
                echo "图片上传失败";
            }
        }else{
            echo "图片获取失败！";
        }

    }
    /**
     * @return 资讯列表修改
     */
    public function informlistedit(){
        if(!request()->isPost()){
            $id = input('id');
            if($id){
                $name= db('information')->where('id='.$id)->find();
                $db = db('information_classification')->select();
                $this->assign('data', $db);
                $this->assign('name',$name);
            }
            return $this->fetch('informlistedit');
        }else {
            $result = [];
            $file = request()->file('file');
            if($file){
                $info = $file->move(ROOT_PATH . 'public'. DS . 'uploadinformimg');
                $filename=$info->getSaveName();

                if($filename){
                    $data = array(
                        'ic_id' => input('post.typename'),
                        'main_title' => input('post.title'),
                        'subtitle' => input('post.titles'),
                        'content'=>input('post.editorValue'),
                        'image'=>$filename,
                        'increase'=>session('a_username'),
                        'sort'=>input('post.sort')

                    );
                    $id = input('post.id');
                    $auth_group = db('information');
                    $auth_group->where('id='.$id)->update($data);

                    $result['info'] = '列表修改成功!';
                    $result['url'] = url('informlist');
                    $result['status'] = 1;
                    return json($result);
                }
            }else{
                $result=[];

                $id = input('post.id');
                $data = array(
                    'ic_id' => input('post.typename'),
                    'main_title' => input('post.title'),
                    'subtitle' => input('post.titles'),
                    'content'=>input('post.editorValue'),
                    'image'=>input('post.img'),
                    'increase'=>session('a_username'),
                    'sort'=>input('post.sort')
                );

                $auth_group = db('information');
                $auth_group->where('id='.$id)->update($data);

                $result['info'] = '列表修改成功!';
//                die;
                $result['url'] = url('informlist');

                $result['status'] = 1;
                return json($result);
            }
        }
    }
    /**
     * @return 资讯列表删除
     */
    public function informlist_del(){
        $id = input('post.id');
        $status= db('information')->where('id='.$id)->select();
        if(!$status){
            $result['status'] = 0;
            $result['info'] = '列表不存在!';
            $result['url'] = url('informlist');
            return json($result);
        }else{
            db('information')->where('id=' . $id)->delete();
            $result['status'] = 1;
            $result['info'] = '删除成功!';
            $result['url'] = url('informlist');
            return json($result);
        }
    }
    /**
     * @return 资讯列表查询单个
     */
    public function inform(){
        $id = input('get.id');
        $admininfo2=db('information')
            ->alias('inform')
            ->where('inform.id=' . $id)
            ->join('information_classification class','class.id=inform.ic_id')
            ->find();
        $this->assign('data', $admininfo2);
        return $this->fetch('inform');
    }
    /**
     * @return 资讯类别查询
     */
    public function iclassification(){
        $db = db('information_classification')->select();
        $this->assign('data', $db);
        return $this->fetch('iclassification');
    }

    public function isiclasstype(){
        $id = input('post.id');
        $info = Db::name('information_classification')
            ->alias('i')
            ->join('information in','in.ic_id=i.id')
            ->where('i.id',$id)
            ->find();
        if ($info&&$info['str']==1){
            return 1;
        }else{
            return 0;
        }
    }

    /**
     * @return 资讯类别修改状态
     */
    public function iclasstype()
    {
        $id = input('post.id');
        $str = input('post.str');
        if (empty($id)) {
            $result['status'] = 0;
            $result['info'] = '用户ID不存在!';
            $result['url'] = url('adminList');
            exit;
        }
        $status = db('information_classification')->where('id=' . $id)->value('str');//判断当前状态情况
        if ($status == 1) {
            $data['str'] = 0;
            db('information_classification')->where('id=' . $id)->update($data);
            $result['status'] = 1;
            $result['info'] = '状态禁止';
        } else {
            $data['str'] = 1;
            db('information_classification')->where('id=' . $id)->update($data);
            $result['status'] = 1;
            $result['info'] = '状态开启';
        }
        return $result;
    }
    /**
     * @return 资讯类别删除状态
     */
    public function type_del(){
        $id = input('post.id');
        $status= db('information_classification')->where('id='.$id)->select();
        if(!$status){
            $result['status'] = 0;
            $result['info'] = '资讯不存在!';
            $result['url'] = url('iclassification');
            exit;
        }else{
            db('information_classification')->where('id=' . $id)->delete();
            $result['status'] = 1;
            $result['info'] = '删除成功!';
            $result['url'] = url('iclassification');
            return json($result);
        }
    }
    /**
     * @return 添加资讯类别
     */
    public function typeAdd(){
        return $this->fetch('typeAdd');
    }
    public function typeInsert()
    {
        $auth_group = db('information_classification');
        $data = array(
            'name' => input('post.titles'),
            'str' => input('post.status'),
            'create_time' => time(),
        );

        $auth_group->insert($data);
        $result['info'] = '用户组添加成功!';
        $result['url'] = url('iclassification');
        $result['status'] = 1;
        return $result;

    }
    /**
     * @return 修改资讯类别
     */
    public function typeEdit(){
        $id = input('get.id');
        if($id){
            $name= db('information_classification')->where('id='.$id)->find();
            $this->assign('name',$name);
        }
        return $this->fetch('typeEdit');
    }
    public function typeupdate(){
        $auth_group = db('information_classification');
        $data = array(
            'name' => input('post.titles'),
            'str' => input('post.status'),
            'update_time' => time(),
        );
        $map['id'] = input('post.id');
        $auth_group->where($map)->update($data);
        $result['info'] = '用户组修改成功!';
        $result['url'] = url('iclassification');
        $result['status'] = 1;
        return $result;
    }

    public function introduction(){
        $text = Db::name('introduction')->find();
        $this->assign('text',$text['introduction']);
//        dump($text['introduction']);die;
        return $this->fetch();
    }

    public function informs(){
//        dump(input('post.editorValue'));
        $text['introduction'] = input('post.editorValue');
        $text['create_time'] = time();
        $info = Db::name('introduction')->select();
        if ($info){
            $info = Db::name('introduction')->where('id',1)->update($text);
            if ($info){
                return jsonmsg(1,'success','');
            }else{
                return jsonmsg(0,'未修改','');
            }
        }else{
            $info = Db::name('introduction')->insert($text);
            if ($info){
                return jsonmsg(1,'success','');
            }else{
                return jsonmsg(0,'error','');
            }
        }
    }


}
