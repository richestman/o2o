<?php
namespace app\home\controller;
use think\Controller;
use think\Db;
use think\Input;

class Goodscate extends Common
{
    public function _initialize(){
        parent::_initialize();
    }

    // 获得产品分类列表
    public function getCateList()
    {
        $list = db('goods_cate')
            ->field('id as typeId, name as typeName, remark as typeRemark')
            ->where('str',1)
            ->select();
        return $list;
    }
    //获得产品状态列表
    public function getStatusList()
    {
        // 保证 下标和状态值 相对应
        // .label label-default	默认的灰色标签
        // .label label-primary	"primary" 类型的蓝色标签
        // .label label-success	"success" 类型的绿色标签
        // .label label-info	"info" 类型的浅蓝色标签
        // .label label-warning	"warning" 类型的黄色标签
        // .label label-danger	"danger" 类型的红色标签
        $list = [];            
        $list[1]=['statusId'=>1,'statusName'=>'已删除','statusColor'=>'label-danger'];
        $list[2]=['statusId'=>2,'statusName'=>'待审核','statusColor'=>'label-default'];
        $list[3]=['statusId'=>3,'statusName'=>'审核中','statusColor'=>'label-info'];
        $list[4]=['statusId'=>4,'statusName'=>'审核通过','statusColor'=>'label-success'];
        $list[5]=['statusId'=>5,'statusName'=>'拒绝','statusColor'=>'label-warning'];
        return $list;
    }
}
