<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/3/28
 * Time: 下午 07:23
 */

namespace app\home\controller;
//use PhpOffice\PhpSpreadsheet\Spreadsheet;

use phpoffice\phpspreadsheet\src\Spreadsheet;
use phpoffice\phpspreadsheet\src\PhpSpreadsheet\Style\Fill;
use phpoffice\phpspreadsheet\src\PhpSpreadsheet\Style\NumberFormat;
use phpoffice\phpspreadsheet\src\PhpSpreadsheet\Style\Color;
use phpoffice\phpspreadsheet\src\PhpSpreadsheet\Style\Alignment;
use phpoffice\phpspreadsheet\src\PhpSpreadsheet\Style\Border;
use phpoffice\phpspreadsheet\src\PhpSpreadsheet\Reader\Xlsx;
use phpoffice\phpspreadsheet\src\PhpSpreadsheet\Reader\Xls;
use phpoffice\phpspreadsheet\src\PhpSpreadsheet\IOFactory;
use phpoffice\phpspreadsheet\src\PhpSpreadsheet\Cell\Coordinate;
//use phpoffice\phpspreadsheet\src\PhpSpreadsheet\Spreadsheet;
use phpoffice\phpspreadsheet\src\PhpSpreadsheet\Worksheet\PageSetup;
use phpoffice\phpspreadsheet\src\PhpSpreadsheet\Cell\DataType;
class Ordermanagement extends Common
{
    public function _initialize()
    {
        parent::_initialize();
    }

    public function management()
    {
        $where = [];
        if (input('post.val')) {
            $where['u.name'] = input('post.val');
        }
        //这是模型切勿删除
//        $data = model('Ordermanagement');
//        $db = $data
//            ->with(['userregistration' => function ($query) {
//                $query->field('id,name');
//            }, 'goods','goods.goodscate'])
//            ->select();
//        return json_encode($db);
//        die;
        $db = db('ordermanagement')
            ->alias('a')
            ->where($where)
            ->join('user_registration u','a.ur_id=u.id')
            ->join('goods g','g.id=a.goods_id')
            ->join('goods_cate ca','ca.id = g.type_id')
            ->field("*,a.id as orid,ca.name as typename,u.name as username,FROM_UNIXTIME(ordertime,'%Y-%m-%d %H:%i:%s') as ordertime")
            ->paginate(10,false,['query' => request()->param()]);
        $page = $db->render();
        $this->assign('data', $db);
        $this->assign('whname',input('post.val'));
        $this->assign('page',$page);
        return $this->fetch();
    }

    public function excelorder(){
        header("Content-type:application/vnd.ms-excel");  //设置内容类型
        header("Content-Disposition:attachment;filename=data.xls");  //文件下载
        $arr = array('类型', '用户', '服务类型', '数量','单价','金额','订单状态','订单日期');
        foreach ($arr as $tval) {
            echo $tval . "\t";
        }
        echo "\n";
        $db = db('ordermanagement')
            ->alias('a')
            ->join('user_registration u','a.ur_id=u.id')
            ->join('goods g','g.id=a.goods_id')
            ->join('goods_cate ca','ca.id = g.type_id')
            ->field("*,ca.name as typename,u.name as username,FROM_UNIXTIME(ordertime,'%Y-%m-%d %H:%i:%s') as ordertime")
            ->select();
        foreach ($db as $key=>$val) {
//            echo iconv('gbk', 'utf-8', $val['type'])."\t";
//            echo iconv('gbk', 'utf-8', $val['username'])."\t";
//            echo iconv('gbk', 'utf-8', $val['typename'])."\t";
//            echo iconv('gbk', 'utf-8', $val['number'])."\t";
//            echo iconv('gbk', 'utf-8', $val['money'])."\t";
//            echo iconv('gbk', 'utf-8', $val['money'])."\t";
//            echo iconv('gbk', 'utf-8', $val['ordertype'])."\t";
//            echo iconv('gbk', 'utf-8', $val['ordertime'])."\t";
            echo $val['type'] . "\t";
            echo $val['username'] . "\t";
            echo $val['typename'] . "\t";
            echo $val['number'] . "\t";
            echo $val['money'] . "\t";
            echo $val['money'] . "\t";
            echo $val['ordertype'] . "\t";
            echo $val['ordertime'] . "\t";
            echo "\n";
        }
        exit();
        vendor("phpoffice.phpexcel.Classes.PHPExcel");
        $obj = new \PHPExcel();
// 文件名和文件类型
        $fileName = "订单导出";
        $fileType = "xlsx";

        // 模拟获取数据
        $data = self::getData();
//        print_r($data);
//        exit();
        // 以下内容是excel文件的信息描述信息
        $obj->getProperties()->setCreator(''); //设置创建者
        $obj->getProperties()->setLastModifiedBy(''); //设置修改者
        $obj->getProperties()->setTitle(''); //设置标题
        $obj->getProperties()->setSubject(''); //设置主题
        $obj->getProperties()->setDescription(''); //设置描述
        $obj->getProperties()->setKeywords('');//设置关键词
        $obj->getProperties()->setCategory('');//设置类型


        // 设置当前sheet
        $obj->setActiveSheetIndex(0);

        // 设置当前sheet的名称
        $obj->getActiveSheet()->setTitle('订单');

        // 列标
        $list = ['A', 'B', 'C'];

        // 填充第一行数据
        $obj->getActiveSheet()
            ->setCellValue($list[0] . '1', '类型')
            ->setCellValue($list[1] . '1', '用户')
            ->setCellValue($list[2] . '1', '服务类别');

        // 填充第n(n>=2, n∈N*)行数据
        $length = count($data);
        for ($i = 0; $i < $length; $i++) {
                $obj->getActiveSheet()->setCellValue($list[0] . ($i + 2), $data[$i]['stuNo'], \PHPExcel_Cell_DataType::TYPE_STRING);//将其设置为文本格式
                $obj->getActiveSheet()->setCellValue($list[1] . ($i + 2), $data[$i]['name']);
                $obj->getActiveSheet()->setCellValue($list[2] . ($i + 2), $data[$i]['class']);

        }

        // 设置加粗和左对齐
        foreach ($list as $col) {
            // 设置第一行加粗
            $obj->getActiveSheet()->getStyle($col . '1')->getFont()->setBold(true);
            // 设置第1-n行，左对齐
            for ($i = 1; $i <= $length + 1; $i++) {
                $obj->getActiveSheet()->getStyle($col . $i)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            }
        }

        // 设置列宽
        $obj->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $obj->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $obj->getActiveSheet()->getColumnDimension('C')->setWidth(15);

        // 导出
        ob_clean();

        if ($fileType == 'xls') {
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=' . $fileName . '.xls');

            header('Cache-Control: max-age=1');
            $objWriter = new \PHPExcel_Writer_Excel5($obj);
            $objWriter->save('php://output');
            exit;
        } elseif ($fileType == 'xlsx') {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename=' . $fileName . '.xlsx');
            header('Cache-Control: max-age=1');

            $objWriter = \PHPExcel_IOFactory::createWriter($obj, 'Excel2007');
            $objWriter->save('php://output');
            exit;
        }
    }
    protected function getData()
    {
        $studentList = [
            [
                'stuNo' => '个人',
                'name' => '倪康盛',
                'class' => '法律'
            ], [
                'stuNo' => '企业',
                'name' => '财经有限公司',
                'class' => '娱乐'
            ], [
                'stuNo' => '个人',
                'name' => '李苏生',
                'class' => '财经'
            ]
        ];

        return $studentList;
    }

    public function details(){
        $where['a.id'] = input('post.id');
        $db = db('ordermanagement')
            ->alias('a')
            ->where($where)
            ->join('user_registration u','a.ur_id=u.id')
            ->join('goods g','g.id=a.goods_id')
            ->join('goods_cate ca','ca.id = g.type_id')
//            ->join('message me','me.order_id = a.id')
            ->field("*,a.id as orid,ca.name as typename,u.name as username,FROM_UNIXTIME(ordertime,'%Y-%m-%d %H:%i:%s') as ordertime")
            ->find();
        $message = db('message')->where('order_id',$db['orid'])->select();
        $db['message']=$message;
        return $db;

    }

    public function message(){
        $data['name'] = input('post.name');
        $data['order_id'] = input('post.id');
        $data['message_content'] = input('post.value');
        $data['time'] = time();
        $info = db('message')->insert($data);
        if ($info){
            return jsonmsg(1,'success',$info);
        }else{
            return jsonmsg(0,'error','');
        }
    }
}