<?php
namespace app\home\controller;
use think\Controller;
use app\home\controller\Common;
use app\home\model\Containers;
class Container extends Common{
   public function index(){
       //实例化类
       $ins = new Containers();
       //使用方法
       $info = $ins->index();
       $this->assign('info',$info);
       $this->assign('page',$info->render());
       return $this->fetch();
   }
   //添加数据
    public function ins(){
       $data = input('post.');
       $data['t_a_id']=session('a_receiveid');
       $data['t_company_id']=session('a_companyid');
       //实例化类
        $ins = new Containers();
        //使用方法
        $info = $ins->ins($data);
        return json_encode($info);
    }
    //删除数据
    public function del(){
       $id = input('id');
        //实例化类
        $ins = new Containers();
        //使用方法
        $info = $ins->del($id);
        return json_encode($info);
    }
    //修改显示
    public function up(){
       $id = input('id');
        //实例化类
        $ins = new Containers();
        //使用方法
        $info = $ins->up($id);
        $this->assign('site',$info);
        return json_encode($info);

    }
    public function xiugai(){
       $data = input('post.');

      //实例化类
       $ins = new Containers();
        //使用方法
       $info = $ins->xiugai($data);
        return json_encode($info);

    }
}
?>