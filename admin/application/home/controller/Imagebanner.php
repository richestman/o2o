<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/3/29
 * Time: 下午 05:57
 */

namespace app\home\controller;


class Imagebanner extends Common
{
    //显示图片上传示例页面
    public function show(){
//        $sid = input("sid");
//        if(!$sid){
//            echo "请将业务id以参数的形式传进来,参数名为sid";
//            exit();
//        }
//        $this->assign("sid",$sid);
//        //查询之前已经上传过的图片
//        $images = db('recruit_image')->where('i_rid',$sid)->select();
//        $this->assign('images',$images);

        $db = db('bannerimage')->field('*,FROM_UNIXTIME(i_time,\'%Y-%m-%d %H:%i:%s\') as i_time')->select();

        $this->assign('image',$db);
        return $this->fetch();
    }


    public function del(){
        $id = input('post.id');
        $where = [
          'id'=>$id
        ];
        $db = db('bannerimage')->where($where)->delete();

        return json_encode($db);
    }



    //上传图片至服务器
    public function uploadImage(){
        $file = request()->file('fileList');
        if($file){
            $info = $file->move(ROOT_PATH . 'public'.DS.'static' . DS . 'uploads');
            $filename=$info->getSaveName();

            if($filename){
                //信息保存至数据库
//                $d['i_rid']=$sid;
                $d['i_image']=$filename;
                $d['i_time']=time();
                $r=db('bannerimage')->insert($d);
                echo $filename;
            }else{
                echo "图片上传失败";
            }
        }else{
            echo "图片获取失败！";
        }
    }
    //上传二维码图片至服务器
    public function qrcodeImage(){
        $file = request()->file('fileList');
        if($file){

            $number = db('qrcode')->count();
            if ($number>=3){
                return "二维码超出上限";
            }else{
                $info = $file->move(ROOT_PATH . 'public'.DS.'static' . DS . 'uploads');
                $filename=$info->getSaveName();

                if($filename){
                    //信息保存至数据库
//                $d['i_rid']=$sid;
                    $d['i_image']=$filename;
                    $d['i_time']=time();
                    $r=db('qrcode')->insert($d);
                    echo $filename;
                }else{
                    echo "图片上传失败";
                }
            }

        }else{
            echo "图片获取失败！";
        }
    }

    //删除图片
    public function deleteImg(){
        $id = input("id");
        $r = db("recruit_image")->where('i_id',$id)->find();
        if($r){
            db("recruit_image")->where('i_id',$id)->delete();
            //删除文件
            $src = ROOT_PATH . 'public'.DS.'static' . DS . 'uploads'.DS.$r['i_image'];
            unlink($src);
            echo 1;
        }else{
            echo 0;
        }
    }


    public function qrcode(){
        $db = db('qrcode')->field('*,FROM_UNIXTIME(i_time,\'%Y-%m-%d %H:%i:%s\') as i_time')->select();

        $this->assign('image',$db);
        return $this->fetch();
    }

    public function delqrcode(){
        $id = input('post.id');
        $where = [
            'id'=>$id
        ];
        $db = db('qrcode')->where($where)->delete();

        return json_encode($db);
    }

}