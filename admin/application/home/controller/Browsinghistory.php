<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/5/22
 * Time: 下午 04:25
 */

namespace app\home\controller;


use think\Controller;

class Browsinghistory extends Common
{

    public function _initialize(){
        parent::_initialize();
    }
    public function index(){
//        $data['user_id'] = input('post.user_id');
//        $data['goods_id'] = input('post.goods_id');
//        $data['create_time'] = time();
        $url = "'".config('urls')."'";
        $goodslist = db('history')->alias('h')
            ->join('goods g', 'h.goods_id = g.id')
            ->join('user_registration u', 'u.id = h.user_id')
            ->field("h.id as id,u.name as uname,g.name as gname,iphone,FROM_UNIXTIME(h.create_time,'%Y-%m-%d %H:%i:%s') as create_time,concat($url,image) as image")
            ->order('id','desc')
            ->paginate(10,false,['query' => request()->param()]);
        $page = $goodslist->render();
        $this->assign('data', $goodslist);
        $this->assign('page',$page);
        return $this->fetch('index');
    }

    public function feedback(){
        $goodslist = db('feedback')
            ->field("*,FROM_UNIXTIME(create_time,'%Y-%m-%d %H:%i:%s') as create_time")
            ->order('id','desc')
            ->paginate(10,false,['query' => request()->param()]);
        $page = $goodslist->render();
        $this->assign('data', $goodslist);
        $this->assign('page',$page);
        return $this->fetch('feedback');
    }



}