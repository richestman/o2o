<?php


namespace app\home\controller;


use think\Controller;

class Userlist extends Common
{
    public function _initialize(){
        parent::_initialize();
    }
    public function userdetails(){
        $where = [];
        if (input('post.val')){
            $where['name'] = input('post.val');
        }
        $db = db('user_registration')
            ->field('*,FROM_UNIXTIME(registration_date,\'%Y-%m-%d %H:%i:%s\') as registration_date')
            ->where($where)
            ->paginate(10,false,['query' => request()->param()]);
//            ->select();
//        echo "<pre>";
//        print_r($db);
//        exit();
        $page = $db->render();
        $this->assign('data', $db);
        $this->assign('page',$page);
        $this->assign('whname',input('post.val'));
        return $this->fetch('userdetails');
    }
    public function starttype(){
        $data = input('post.');
        $id = $data['id'];

        $db = db('user_registration')->where('id',$id)->value('yesno');

        if ($db==1){
            db('user_registration')->where('id',$id)->update(['yesno'=>'0']);
            return '0';
        }
        if ($db==0){
            db('user_registration')->where('id',$id)->update(['yesno'=>'1']);
            return '1';
        }
    }
}
