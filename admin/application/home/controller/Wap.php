<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2018/12/15
 * Time: 17:01
 */

namespace app\home\controller;


use think\Controller;

class Wap extends Controller
{
    public function index()
    {
        $iteam = input('id');
        $ite = input('comid');
        $where=[
            't_ticket_no'=> $iteam,
            't_companyid'=> $ite
        ];
        $info = db('track')->where($where)->order('t_id desc')->select();
        foreach ($info as &$v){
            $v['t_createtime']=date('Y-m-d h:i:s',$v['t_createtime']);
        }
        $this->assign('info', $info);
        return $this->fetch();
    }
}