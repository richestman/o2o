<?php
namespace app\home\controller;
use think\Controller;
use think\Request;
use think\Session;
use app\home\model\Macaddr;
class Loginb extends Controller
{
    private $sysConfig ,$cache_model,$siteConfig,$menudata ;
    function _initialize()
    {

    }
    public function check($code = '')
    {
        if (!captcha_check($code)) {
            $this->error('验证码错误');
        } else {
            return true;
        }
    }
    public function login()
    {
        //判断管理员是否登录
        $aid = session('aid');
        if($aid){
            $this->redirect('index/index');
        }
        return $this->fetch('login/login2');
    }
    public function login2(){
        return $this->fetch();
    }

    public function test(){
        return $this->fetch();
//        $getmac=new Macaddr();
//        $mac=$getmac->GetMac(PHP_OS);   //这个为数组，里面存放的是本机所有的mac地址
//        print_r($mac);
//        exit();
//        $mac = array();
//        $array=array();
//        @exec("arp -a",$array); //执行arp -a命令，结果放到数组$array中
//        foreach($array as $value){
//            if(strpos($value,$_SERVER["REMOTE_ADDR"]) && preg_match("/(:?[0-9A-F]{2}[:-]){5}[0-9A-F]{2}/i",$value,$mac_array)){
//                $mac = $mac_array[0];
//                break;
//            }
//        }
//        print_r($mac);
    }

    public function action(){
        $admin = db('c_admin');
        $map['a_username']=input('username');
        $mac = input('mac'); //接受客户端传过来的mac
        $map['a_is_open']=1;
        $this->check(input('code'));
        $password=md5(input('password'));
        $admininfo=$admin->where($map)->find();
        $istrue='1';
        if (!$admininfo){
            $this->error('用户名错误，重新输入');
            exit();
        }
        //密码
        elseif($password != $admininfo['a_password']){
            $this->error('密码错误，重新输入');
            exit();
        }else{

            //查询该账号所在公司
            $admininfo2=db('c_admin')
                ->alias('ca')
                ->join('company c','ca.a_companyid=c.c_id')
                ->where('ca.a_id='.$admininfo['a_id'])
                ->field('c_is_usermac,c.c_id, c.c_name, ca.a_receiveid')
                ->find();
            $area_id = $admininfo2['a_receiveid'];
            $a_ismac=$admininfo2['c_is_usermac'];
            $companyid=$admininfo2['c_id'];
            $company_name = $admininfo2['c_name'];
            //获取地区名称
            $area_name = db('area')
                ->where("a_id = '$area_id'")
                ->value('a_name');
            if($area_id == '') {
                $area_name = "超级管理员";
            }

            if($a_ismac==2){
                //获取本机电脑mac地址
//                $getmac=new Macaddr();
//                $mac=$getmac->GetMac(PHP_OS);   //这个为数组，里面存放的是本机所有的mac地址
                if(empty($mac)){
                    $result['status'] = 2;
                    $result['msg'] = '您的系统已经开启授权限制，请调换您的浏览器设置!';
                    return $result;
                }
                //查询本机是否被授权
                $macs=db('company_mac')
                    ->alias('cm')
                    ->join('company c','c.c_id=cm.m_cid')
                    ->where("m_cid='$companyid'")
                    ->field('m_mac')
                    ->select();


                foreach($macs as $k=>$v){
                    if($v['m_mac']==$mac){
                        $istrue = 2;
                        break;
                    }
                   // $mac_arr[]=$v['m_mac'];
                }
//                foreach($mac as $k=>$v){
//                    if(in_array($v,$mac_arr)){
//                        $istrue=2;
//                        break;
//                    }else{
//                        $istrue=1;
//                    }
//                }


            }else{
                $istrue=2;
            }


            if($istrue==2){

                    //登录后更新数据库，登录IP，登录次数,登录时间
                    $data['a_ip'] = Request::instance()->ip();
                    $where['a_id'] = $admininfo['a_id'];
                    $admin->where($where)->setInc('a_hits',1);
                    $admin->where($where)->update($data);
                    Session::set('aid',$admininfo['a_id']);
                    Session::set('a_companyid',$admininfo['a_companyid']);
                    Session::set('a_username',$admininfo['a_username']);
                    Session::set('a_codename',$admininfo['a_codename']);
                    Session::set('a_receiveid',$admininfo['a_receiveid']);
                    Session::set('a_company_name', $company_name);
                    Session::set('area_name', $area_name);
                    $result['status'] = 1;
                    $result['msg'] = '恭喜您，登陆成功!';
                    return $result;

            }else{
                $result['status'] = 2;
                $result['msg'] = '该电脑未被授权!无法登陆!';
                return $result;
            }
        }
    }
    //退出登陆
    public function logout(){
        session(null);
        $this->redirect('login/login');
    }
}