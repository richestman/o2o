<?php
namespace app\home\controller;
use think\Controller;
use think\Db;
use think\Input;

class Goods extends Goodscate
{
    public function _initialize(){
        parent::_initialize();
    }
    /**
     * 商品列表页
     */
    public function index()
    {

        $valuewhere = input('valuewhere','');
        $status = input('status','');
        $type = input('type','');
        $where = array();
        if ($valuewhere) {
            $where['g.name|g.introduce'] = array('like', "%" . $valuewhere . "%");
        }
        if ($status){
            $where['status'] = $status;
        }
        if ($type){
            $where['type_id'] = $type;
        }
        // $url['val'] = $val;
        $this->assign('valuewhere', $valuewhere);
        $this->assign('status', $status);
        $this->assign('type', $type);
        $goodslist = db('goods')->alias('g')
            ->join('goods_cate gc', 'g.type_id = gc.id')
            ->join('c_admin cd', 'cd.a_id = g.creater')
            ->where($where)
            ->field("g.*, gc.name as typename ,gc.str,cd.a_username as creater, FROM_UNIXTIME(create_time,'%Y-%m-%d %H:%i:%s') as create_time")
//            ->order('sort','asc')
            ->order('id','desc')
            // ->paginate(config('pageSize'), false, ['query' => request()->param()])
            ->paginate(10, false, ['query' => request()->param()]);
        // $goodslist->appends($url);
        $cateList = $this->getCateList();//分类列表
        $statusList = $this->getStatusList();//状态列表
        // 模板变量赋值
        $page = $goodslist->render();
        $this->assign('goodslist',$goodslist);//商品列表
        $this->assign('catelist',$cateList);
        $this->assign('statuslist',$statusList);
        $this->assign('page',$page);
        return $this->fetch('index');
    }

    /**
     * 增加商品页
     */
    public function goodsAdd() {
        $cateList = $this->getCateList();//分类列表
        $this->assign('catelist',$cateList);
        return $this->fetch('goodsAdd');
    }

    public function goodsInsert() {
        // $test = input('post.');
        // return callback(2,'2','2',$test);
        $data['name'] = input('name');
        $data['type_id'] = input('type_id');
        $data['sort'] = input('sort');
        $data['price'] = input('price');
        // $data['price_old'] = input('price_old');
        $data['image'] = '';
        $data['introduce'] = input('introduce');
        $data['introduction'] = input('introduction');
        $data['process'] = input('process');
        $data['word'] = '暂无';//;input('word');
        $data['creater'] = session('aid');
        $data['create_time'] = time();
        $data['status'] = 4;//通过
        $image = request()->file('image');
        $files = request()->file('agreement');


        if($image && $files){
            $info = $image->move(ROOT_PATH . 'public'. DS . 'uploadinformimg');
            $imageFileName=$info->getSaveName();
            $infoFiles = $files->move(ROOT_PATH . 'public'. DS . 'uploadinformimg');
            $filesFileName=$infoFiles->getSaveName();
            if($imageFileName && $filesFileName){
                $data['image'] = $imageFileName;
                $data['word'] = $filesFileName;
                $data['wordname'] = $_FILES['agreement']['name'];

                 $AddInfo = db('goods')->insert($data);

                return callback(1,'增加成功','index',$data);
            }else{
                return callback(3,'文件上传失败','',$image);
            }
        }else{
            return callback(2,'文件获取失败','',$image);
        }
    }
    public function goodsEditSave(){
        $data['name'] = input('name');
        $data['type_id'] = input('type_id');
        $data['price'] = input('price');
        $data['sort'] = input('sort');
        // $data['price_old'] = input('price_old');
        $data['image'] = '';
        $data['introduce'] = input('introduce');
        $data['introduction'] = input('introduction');
        $data['process'] = input('process');
        $data['word'] = '暂无';//;input('word');
        $data['checker'] = session('aid');
        $data['check_time'] = time();
        $imageEdit = input('imageedit');
        $id = input('id');
        $image = request()->file('image');
        $files = request()->file('agreement');//新文件

        $result = [];
        $file = request()->file('image');
        if($file){
            $info = $file->move(ROOT_PATH . 'public'. DS . 'uploadinformimg');
            $filename=$info->getSaveName();
            if($filename){
                $data['image'] =$filename;
            }
        }else{
            $data['image'] = input('post.img');
        }
//        文件
        if($files){
            $infoWord = $files->move(ROOT_PATH . 'public'. DS . 'uploadinformimg');
            $filenameWord=$infoWord->getSaveName();
            if($filenameWord){
                $data['word'] =$filenameWord;
                $data['wordname'] = $_FILES['agreement']['name'];
            }
        }else{
            $data['word'] = input('word');
            $data['wordname'] = input('wordname');
        }
        $auth_group = db('goods');
        $auth_group->where('id',$id)->update($data);
        $result['info'] = '列表修改成功!';
        $result['url'] = url('index');
        $result['status'] = 1;
        $result['data'] = $data;
        return json($result);
    }
    public function goods_del(){
      $id = input('post.id');
        $status= db('goods')->where('id',$id)->find();
        if(!$status){
            $result['status'] = 0;
            $result['info'] = '服务不存在!';
            $result['url'] = url('cate');
            return json($result);
        }else{
            db('goods')->where('id',$id)->delete();
            $result['status'] = 1;
            $result['info'] = '删除成功!';
            $result['url'] = url('cate');
            return json($result);
        }
    }
    public function GoodsEdit(){
      $id = input('get.id');
      if($id){
          $info= db('goods')->where('id',$id)->find();
          $cateList = $this->getCateList();//分类列表
          $this->assign('catelist',$cateList);
          $this->assign('goodsInfo',$info);
      }
      return $this->fetch('goodsEdit');
  }
    public function cate(){
        $db = db('goods_cate')->select();
        $this->assign('data', $db);
        return $this->fetch();
    }

    public function isiclasstype(){
        $id = input('post.id');
        $info = Db::name('goods_cate')
            ->alias('g')
            ->join('goods s','s.type_id=g.id')
            ->where('g.id',$id)
            ->find();
        if ($info&&$info['str']==1){
            return 1;
        }else{
            return 0;
        }

    }


    /**
     * @return 产品类别修改状态
     */
    public function iclasstype()
    {
        $id = input('post.id');
        $str = input('post.str');
        if (empty($id)) {
            $result['status'] = 0;
            $result['info'] = '用户ID不存在!';
            $result['url'] = url('adminList');
            exit;
        }
        $status = db('goods_cate')->where('id=' . $id)->value('str');//判断当前状态情况
        if ($status == 1) {
            $data['str'] = 0;
            db('goods_cate')->where('id=' . $id)->update($data);
            $result['status'] = 1;
            $result['info'] = '状态禁止';
        } else {
            $data['str'] = 1;
            db('goods_cate')->where('id=' . $id)->update($data);
            $result['status'] = 1;
            $result['info'] = '状态开启';
        }
        return $result;
    }
    /**
     * @return 添加资讯类别
     */
    public function typeAdd(){
        return $this->fetch('typeAdd');
    }

    public function typeInsert()
    {
        $auth_group = db('goods_cate');
        $data = array(
            'name' => input('post.titles'),
            'str' => input('post.status'),
//            'create_time' => time(),
        );

        $auth_group->insert($data);
        $result['info'] = '用户组添加成功!';
        $result['url'] = url('cate');
        $result['status'] = 1;
        return $result;

    }
    /**
     * @return 资讯类别删除状态
     */
    public function type_del(){
        $id = input('post.id');
        $status= db('goods_cate')->where('id='.$id)->select();
        if(!$status){
            $result['status'] = 0;
            $result['info'] = '资讯不存在!';
            $result['url'] = url('cate');
            exit;
        }else{
            db('goods_cate')->where('id=' . $id)->delete();
            $result['status'] = 1;
            $result['info'] = '删除成功!';
            $result['url'] = url('cate');
            return json($result);
        }
    }





    /**
     * @return 修改资讯类别
     */
    public function typeEdit(){
        $id = input('get.id');
        if($id){
            $name= db('goods_cate')->where('id='.$id)->find();
            $this->assign('name',$name);
        }
        return $this->fetch('typeEdit');
    }

    public function typeupdate(){
        $auth_group = db('goods_cate');
        $data = array(
            'name' => input('post.titles'),
            'str' => input('post.status'),
//            'update_time' => time(),
        );
        $map['id'] = input('post.id');
        $auth_group->where($map)->update($data);
        $result['info'] = '用户组修改成功!';
        $result['url'] = url('cate');
        $result['status'] = 1;
        return $result;
    }
    
}
