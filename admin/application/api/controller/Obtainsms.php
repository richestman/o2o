<?php


namespace app\api\controller;
//use app\common\controller\Common;
use think\Controller;
//use app\common\controller\Sms;
use think\Db;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE");
header('Access-Control-Allow-Headers:x-requested-with,content-type');
class Obtainsms extends Controller
{
    public function index(){
        $tel = input('post.iphone');
//        $tel = '15698280591';
        $rand = $this->randcode();
        $code = ['code' => $rand];
        $signName='扶持通';
        $sms = new AliyunSendSms();
        $mobile = $tel;
        $TemplateCode='SMS_189763478';
        $info = $sms->sendSms($mobile,$code,$signName,$TemplateCode);
//        $info['Code'] = 'OK';
        if ($info['Code']=='OK'){
            $info = $this->qrcode($tel,$rand);
            if ($info){
                return jsonmsg(1, '验证码发送成功', '');
            }else{
                return jsonmsg(0, '验证码发送失败', '');
            }
        }else{
            return jsonmsg(0, '验证码发送失败', '');
        }

    }


    /**
     * @return int 生成随机数6位
     */
    public function randcode(){
        return mt_rand(100000,999999);
    }

    /**
     * @param $tel
     * @param $rand
     * @return int|string 验证码写入
     */
    public function qrcode($tel,$rand){
        $data['tel']=$tel;
        $data['captcha']=$rand;
        $data['create_time']=time();
        $info = Db::name('captcha')->insert($data);
//        $isPresence = $this->isPresence($tel['mobile']);
//        if ($isPresence){
//            $info = Db::name('tel')->where('mobile',$tel['mobile'])->update($data);
//        }else{
//            $info = Db::name('tel')->insert($data);
//        }
        return jsonmsg(1,'success',$info);
    }

    /**
     * @param $mobile
     * @return array|false|\PDOStatement|string|\think\Model
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException 查看手机号是否存在
     */
    public function isPresence($mobile){
        $info = Db::name('tel')->where('mobile',$mobile)->find();
        return $info;
    }

}