<?php


namespace app\api\controller;
use think\Controller;
use think\Db;
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: token,Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: POST,GET');

class Gsadder extends Controller
{
    public function gsadder(){
        $data = Db::name('adder')->find();
        return Json($data);
    }
}