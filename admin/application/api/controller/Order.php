<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/5/5
 * Time: 上午 12:12
 */

namespace app\api\controller;


use think\Controller;
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: token,Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: POST,GET');
class Order extends Controller
{

    public function index(){
        $id = input('post.id');
        $page = input('post.page')?input('post.page'):1;
        $limit=input('post.number')?input('post.number'):10;
        $end = ($page-1)*$limit+$limit;
        $ope =($page-1)*$limit;
        $where['u.id']=$id;

        $db = db('ordermanagement')
            ->alias('a')
            ->where($where)
            ->join('user_registration u','a.ur_id=u.id')
            ->join('goods g','g.id=a.goods_id')
            ->join('goods_cate ca','ca.id = g.type_id')
            ->field("*,a.id as orid,ca.name as typename,u.name as username,FROM_UNIXTIME(ordertime,'%Y-%m-%d %H:%i:%s') as ordertime")
            ->limit($ope,$limit)
            ->select();
        if ($db){
            return jsonmsg(1,'seccess',$db);
        }else{
            return jsonmsg(0,'error','');
        }
    }

    public function addorder(){
        $data['ur_id'] = input('post.userid');
        $data['goods_id'] = input('post.goodsid');
        $data['number'] = input('post.number');
        $data['money'] = input('post.money');
        $data['ordertype'] = 1;
        $data['ordertime'] = time();

        $info = db('ordermanagement')->insert($data);
        if ($info){
            return jsonmsg(1,'success','');
        }else{
            return jsonmsg(0,'error','');
        }
    }
    public function message(){
        $where['a.id'] = input('post.id');
        $db = db('ordermanagement')
            ->alias('a')
            ->where($where)
            ->join('user_registration u','a.ur_id=u.id')
            ->join('goods g','g.id=a.goods_id')
            ->join('goods_cate ca','ca.id = g.type_id')
//            ->join('message me','me.order_id = a.id')
            ->field("*,a.id as orid,ca.name as typename,u.name as username,FROM_UNIXTIME(ordertime,'%Y-%m-%d %H:%i:%s') as ordertime")
            ->find();
        $message = db('message')->where('order_id',$db['orid'])->select();
        $db['message']=$message;
        if ($db && $message){
            return jsonmsg(1,'success',$db['message']);
        }else{
            return jsonmsg(0,'error','');
        }

    }

    public function addmessage(){
        $data['name'] = input('post.name');
        $data['order_id'] = input('post.id');
        $data['message_content'] = input('post.value');
        $data['time'] = time();
        $info = db('message')->insert($data);
        if ($info){
            return jsonmsg(1,'success',$info);
        }else{
            return jsonmsg(0,'error','');
        }
    }

}