<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/5/22
 * Time: 下午 05:36
 */

namespace app\api\controller;


use think\Controller;
use think\Db;
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE");
header('Access-Control-Allow-Headers:x-requested-with,content-type');
class Feedback extends Controller
{
    public function index(){
        $data = input('post.');
        $data['create_time'] = time();
        $info = Db::name('feedback')->insert($data);
        if ($info){
            return jsonmsg(1,'success','');
        }else{
            return jsonmsg(0,'error','');
        }

    }
}