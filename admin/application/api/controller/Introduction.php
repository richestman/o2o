<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/5/23
 * Time: 下午 05:38
 */

namespace app\api\controller;


use think\Controller;
use think\Db;
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: token,Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: POST,GET');
class Introduction extends Controller
{

    public function index(){
        $info = Db::name('introduction')->find();
        return jsonmsg(1,'success',$info);
    }
}