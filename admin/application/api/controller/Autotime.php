<?php

namespace app\api\controller;

use think\Controller;

class Autotime extends Controller
{
    //每天下午4点定时封单提交
    public function submitReceive()
    {
        ignore_user_abort();//关掉浏览器，PHP脚本也可以继续执行.
        set_time_limit(0);// 通过set_time_limit(0)可以让程序无限制的执行下去
        $interval = 60 * 5;// 每隔五分钟运行

        do {

            if (!config('autosql')) die('process abort');

            //ToDo
            $r = db("test")->where("id='1'")->find();
            $n = $r['num'] + 1;
            $k = db("test")->where("id='1'")->update(['num' => $n]);
            $nowtime = date('H') * 60 + date('i');
            echo $nowtime;
            if ($nowtime > 950 && $nowtime < 960) {
                //下午的三点五十到到四点之间
                $data = [];
                $info = db("receive")->where("r_isclose='1'")->where("r_autosubmit='1'")->select();
                foreach ($info as $value) {
                    $data['t_goods_no'] = $value['r_goods_no'];
                    $data['t_ticket_no'] = $value['r_ticket_no'];
                    $data['t_companyid'] = $value['r_company_id'];
                    $data['t_operator'] = $value['r_operator'];
                    $data['t_createtime'] = strtotime("now");
                    $data['t_type'] = '封单提交';
                    $data['t_desc'] = '16：00自动封单提交';
                    db("track")->insert($data);
                }
                //除了改货物为封单状态，还要加上封单时间
                db("receive")->where("r_isclose='1'")->where("r_autosubmit='1'")->update(['r_isclose' => '2','r_seal_time'=>time()]);

            }
            sleep($interval);// 等待5分钟
        } while (true);
    }

}