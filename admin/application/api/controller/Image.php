<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/4/22
 * Time: 下午 11:47
 */

namespace app\api\controller;

use think\Controller;
use think\Db;

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: token,Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: POST,GET');
class Image extends Controller
{
    /**
     * @return false|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException banner 轮播图
     */
    public function banner(){
        $url = "'".config('url')."'";
        $image = Db::name('bannerimage')->field("*,concat($url,i_image) as images")->select();
        if ($image){
            return json_encode(['code'=>'1','msg'=>'success','data'=>$image]);
        }else{
            return json_encode(['code'=>'0','msg'=>'error','data'=>'']);
        }
    }
    public function qrcode(){
        $url = "'".config('url')."'";
        $image = Db::name('qrcode')->field("*,concat($url,i_image) as images")->select();
        if ($image){
            return json_encode(['code'=>'1','msg'=>'success','data'=>$image]);
        }else{
            return json_encode(['code'=>'0','msg'=>'error','data'=>'']);
        }
    }
}