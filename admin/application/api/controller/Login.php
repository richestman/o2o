<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/4/7
 * Time: 下午 10:16
 */

namespace app\api\controller;


use app\api\model\Token;
use app\commom\common;
use think\Controller;
use think\Db;

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: token,Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: POST,GET');

class Login extends Controller
{

    public function __construct()
    {
//        $data = '';
//        foreach ($_SERVER as $k => $value) {
//            if ($k == 'HTTP_TOKEN') {
//                $data = $value;
//                $token = new Token();
//                $token = $token->verificationToken($data);
//                return $token;
//            }
//        }
    }

    public function index()
    {

        $username = input('post.name');
        $password = md5(input('post.password'));

        $where = [
            'name'=>$username,
            'password'=>$password
        ];
        $info = Db::name('user_registration')->where($where)->select();

        if($info){
            $token = new Token();
            $token = $token->generateToken();
            return $token;
        }else{
            $arry = [
              'code'=>0,
              'msg'=>'用户不存在'
            ];
            return json_encode($arry);
        }





    }

    public function verification()
    {


        $data = '';
        foreach ($_SERVER as $k => $value) {
            if ($k == 'HTTP_TOKEN') {
                $data = $value;
                $token = new Token();
                $token = $token->verificationToken($data);
                return $token;
            }
        }
    }

    public function login(){
        $name = input('post.name');
        $password = md5(input('post.password'));
//        $captcha = input('post.captcha');
//        $iphone = input('post.iphone');
        $where = [
            'iphone'=>$name,
            'password'=>$password
        ];
        $info = Db::name('user_registration')->where($where)->select();
        if($info){
            return jsonmsg(1,'success',$info);
        }else{
            return jsonmsg(0,'error','');
        }
    }
    public function registered(){
        $name = input('post.name');
        $pwd = input('post.pwd');
        $password = md5($pwd);
        $captcha = input('post.captcha');
        $iphone = input('post.iphone');
        if ($password&&$captcha&&$iphone){
//这里要写验证码
            $te['tel'] = $iphone;
            $te['captcha'] = $captcha;
            $tel = Db::name('captcha')->where($te)->find();
            if ($tel){
                $time = '5';
                //计算天数
                $timediff = time()-$tel['create_time'];
                $days = intval($timediff/86400);
                //计算小时数
                $remain = $timediff%86400;
                $hours = intval($remain/3600);
                //计算分钟数
                $remain = $remain%3600;
                $mins = intval($remain/60);
                if ($mins>$time||$hours>=1||$days>=1){
                    return jsonmsg(0,'验证码过期','');
                }else{

                    $data['iphone']=$iphone;
                    $data['name']=$name;
                    $data['password']=$password;
                    $isse = Db::name('user_registration')->where('iphone',$iphone)->find();

                    if ($isse){
                        return jsonmsg(0,'error','');
                    }else{
                        $info = Db::name('user_registration')->insertGetId($data);
                        $info = Db::name('user_registration')->where('id',$info)->find();
                        return jsonmsg(1,'success',$info);
                    }

                }

            }else{
                return jsonmsg(0,'error','');
            }




        }
    }
    public function captcha($captcha)
    {
        $time = '5';
        $info = Db::name('tel')->where($captcha)->find();
        if ($info){
            //计算天数
            $timediff = time()-$info['creat_time'];
            $days = intval($timediff/86400);
            //计算小时数
            $remain = $timediff%86400;
            $hours = intval($remain/3600);
            //计算分钟数
            $remain = $remain%3600;
            $mins = intval($remain/60);
            if ($mins>$time||$hours>=1||$days>=1){
                return 0;
            }else{
                return $info;
            }
        }else{
            return $info;
        }
    }

}