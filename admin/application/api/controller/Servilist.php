<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 2020/4/23
 * Time: 上午 12:29
 */

namespace app\api\controller;


use think\Controller;
use think\Db;
use think\Request;
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: token,Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: POST,GET');
class Servilist extends Controller
{

    /**
     * @return false|string get请求
     * page =>页数
     * number=>每页显示数
     */

    public function index(Request $request){
        $url = "'".config('urls')."'";
        $page = input('get.page')?input('get.page'):1;
        $limit=input('get.number')?input('get.number'):10;
        $end = (($page-1)*$limit)+$limit;
        $ope =(($page-1)*$limit);
        $where=[];
        input('get.type','')?$where['g.type_id']=input('get.type'):'';
        input('get.value','')?$where['g.name']=['like','%'.input('get.value').'%']:'';
        $where['str']=1;
        $goodslist = db('goods')->alias('g')
            ->join('goods_cate gc', 'g.type_id = gc.id')
            ->join('c_admin cd', 'cd.a_id = g.creater')
            ->where($where)
            ->field("g.*, gc.name as typename ,cd.a_username as creater, FROM_UNIXTIME(create_time,'%Y-%m-%d %H:%i:%s') as create_time,concat($url,image) as image,concat($url,word) as word")
            ->order('sort','asc')
            ->order('id','desc')
            ->limit($ope,$limit)
            ->select();
        $goodslistcount = db('goods')->alias('g')
            ->join('goods_cate gc', 'g.type_id = gc.id')
            ->join('c_admin cd', 'cd.a_id = g.creater')
            ->where($where)
            ->count();
        $count = $goodslistcount;
//        return json_encode($goodslist);
        return json_encode(['code'=>1,'msg'=>'success','data'=>$goodslist,'count'=>$count]);
    }

    public function getCateList()
    {
        $list = db('goods_cate')->field('id as value, name as text, remark as remark')->select();
        return jsonmsg('1','success',$list);
    }

    public function informationType(){
        $list = db('information_classification')->select();
        return jsonmsg('1','success',$list);
    }
    public function information(){
        $url = "'".config('urls')."'";
        $id = input('post.id');
        $page = input('post.page')?input('post.page'):1;
        $limit=input('post.number')?input('post.number'):10;
        $end = (($page-1)*$limit)+$limit;
        $ope =(($page-1)*$limit);
        $where = [];
        $where['str'] = 1;
        if ($id){
            $where['ic_id']=$id;
        }
        $list = db('information')
            ->alias('i')
            ->join('information_classification in','in.id=i.ic_id')
            ->where($where)
            ->field("*,FROM_UNIXTIME(addtime,'%Y-%m-%d %H:%i:%s') as addtime,concat($url,image) as image")
            ->order('sort','asc')
            ->limit($ope,$limit)
            ->select();
        $listcount = db('information')
            ->alias('i')
            ->join('information_classification in','in.id=i.ic_id')
            ->where($where)
            ->count();
        $count = $listcount;
        return json_encode(['code'=>1,'msg'=>'success','data'=>$list,'count'=>$count]);
    }

    public function goodsid(){
        $id = input('post.id');
        $where = [];
        if ($id){
            $where['g.id']=$id;
        }
        $goodslist = db('goods')->alias('g')
            ->join('goods_cate gc', 'g.type_id = gc.id')
            ->join('c_admin cd', 'cd.a_id = g.creater')
            ->where($where)
            ->field("g.*, gc.name as typename ,cd.a_username as creater, FROM_UNIXTIME(create_time,'%Y-%m-%d %H:%i:%s') as create_time")
//            ->order('id','asc')
            ->select();
        return jsonmsg('1','success',$goodslist);
    }

}