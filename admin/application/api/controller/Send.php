<?php
/**
 * 下行协议
 * 数据方向：合作方 ->空间畅想短信网关。
 * 通信方式：HTTP GET或POST
 * @author zll
 */
namespace app\api\controller;
use think\Controller;
class Send{
    private $name = 'lydzhy';//账号
    private $password = 'dazhi123!a';//密码


    private $enCode = 'UTF-8';//编码（UTF-8、GBK）
    private $method = 'GET';//请求方式（POST、GET）
    private $url = 'http://160.19.212.218:8080/eums/utf8/send_strong.do';


    //发送短信
    public function sendsms(){
        $dest = input("dest",'18306505051');         //手机号码
        $content = input("content",'【助友物流收货提醒】测试');  //接收到的短信内容
        $content  = $this->encoding($content,$this->enCode);//注意编码，字段编码要和接口所用编码一致，有可能出现汉字之类的记得转换编码

        //请求参数
        //utf8和gbk编码请自行转换
        $params = array(
            'name' => $this->encoding($this->name,$this->enCode),//帐号，由网关分配
            'seed' => date("YmdHis"),//当前时间，格式：YYYYMMDD HHMMSS，例如：20130806102030。客户时间早于或晚于网关时间超过30分钟，则网关拒绝提交。
            //从php5.1.0开始，PHP.ini里加了date.timezone这个选项，并且默认情况下是关闭的也就是显示的时间（无论用什么php命令）都是格林威治标准时间和我们的时间（北京时间）差了正好8个小时。
            //找到php.ini中的“;date.timezone =”这行，将“;”去掉，改成“date.timezone = PRC”（PRC：People's Republic of China 中华人民共和国），重启Apache，问题解决。
            'key' => md5(md5($this->password). date("YmdHis")),//md5( md5(password)  +  seed) )
            //其中“+”表示字符串连接。即：先对密码进行md5加密，将结果与seed值合并，再进行一次md5加密。
            //两次md5加密后字符串都需转为小写。
            //例如：若当前时间为2013-08-06 10:20:30，密码为123456，
            //则：key=md5(md5(“123456”) + “20130806102030” )
            //则：key=md5(e10adc3949ba59abbe56e057f20f883e20130806102030)
            //则：key= cd6e1aa6b89e8e413867b33811e70153
            'dest' => $dest,//手机号码（多个号码用“半角逗号”分开），GET方式每次最多100个号码，POST方式号码个数不限制，但建议不超过3万个
            'content' => $content,//短信内容。最多500个字符。
            'ext' => '',//扩展号码（视通道是否支持扩展，可以为空或不填）
            'reference' => ''//参考信息（最多50个字符，在推送状态报告、推送上行时，推送给合作方，本参数不参与任何下行控制，仅为合作方提供方便，可以为空或不填）如果不知道如何使用，请忽略该参数，不能含有半角的逗号和分号。
        );


        $resp =  $this->send_get($this->url, $params);//GET请求，数据返回格式为error:xxx,success:xxx

        $response = explode(':', $resp);
        $code = $response[1];//响应代码
        if ($response[0]=='success'){
            echo '成功代码:'.$code.'<br/>';
        }else{
            echo '错误代码:'.$code.'<br/>';
        }
    }


    //编码函数
    private function encoding($str,$urlCode){
        if( !empty($str) ){
            $fileType = mb_detect_encoding($str , array('UTF-8','GBK','LATIN1','BIG5')) ;
        }
        return mb_convert_encoding($str, $urlCode, $fileType);
    }

    /**
     * get请求
     */
    private function send_get($url,$params){
        $getdata = http_build_query($params);
        $content = file_get_contents($url.'?'.$getdata);
        return $content;
    }

    /**
     * post请求
     * @param $url
     * @param $params
     */
    private function send_post_curl($url,$params){

        $postdata = http_build_query($params);
        $length = strlen($postdata);
        $cl = curl_init($url);//①：初始化
        curl_setopt($cl, CURLOPT_POST, true);//②：设置属性
        curl_setopt($cl,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
        curl_setopt($cl,CURLOPT_HTTPHEADER,array("Content-Type: application/x-www-form-urlencoded","Content-length: ".$length));
        curl_setopt($cl,CURLOPT_POSTFIELDS,$postdata);
        curl_setopt($cl,CURLOPT_RETURNTRANSFER,true);
        $content = curl_exec($cl);//③：执行并获取结果
        curl_close($cl);//④：释放句柄
        return $content;
    }
}












