<?php

namespace app\home\controller;

use app\home\model\Track;
use think\Controller;
use think\Db;
use think\Input;
use app\home\model\Receivem;
use think\response\Json;

class Receive extends Common
{
    public function _initialize()
    {
        parent::_initialize();
    }

    //显示收货单页面
    public function index()
    {
        $company = session('a_companyid');
        $ses = session('a_receiveid');
        $name = session('a_username');
        $admin = db('c_admin')->where("a_username='$name'")->find();
        $a = [];
        $b = [];
        $where = [
            'a_company_id' => $company,
            'a_pid' => ['=', 0]
        ];
        $area = db('area')->where($where)->select();
        foreach ($area as $key => &$v) {
            $v['secondlist'] = db('area')->where('a_pid=' . $v['a_id'])->select();
            //遍历3级
            foreach ($v['secondlist'] as $k => &$t) {
                $t['thirdlist'] = db('area')->where('a_pid=' . $t['a_id'])->select();
                //遍历4级

                foreach ($t['thirdlist'] as $kk1 => $vv1) {
                    array_push($b, $vv1['a_name'] . $vv1['a_engname']);
                }
                foreach ($t['thirdlist'] as $k => &$f) {
                    $f['for'] = db('area')->where('a_pid=' . $f['a_id'])->select();

                    foreach ($f['for'] as $kk => $vv) {
                        array_push($a, $vv['a_name'] . $vv['a_engname']);
                    }
                }
            }
        }

        if (empty($a)) {

            $this->assign('depart', $b);

        } else {
            $this->assign('depart', $a);
        }
        $ship = db('area')->where("a_id='$ses'")->find();
        $sh = $ship['a_end_addr'];
        $this->assign('shi', $sh);
        return $this->fetch();


    }


    public function indexs()
    {
        $company = session('a_companyid');
        $ses = session('a_receiveid');
        $name = session('a_username');
        $admin = db('c_admin')->where("a_username='$name'")->find();
        $a = [];
        $b = [];
        $where = [
            'a_company_id' => $company,
            'a_pid' => ['=', 0]
        ];
        $area = db('area')->where($where)->select();
        foreach ($area as $key => &$v) {
            $v['secondlist'] = db('area')->where('a_pid=' . $v['a_id'])->select();
            //遍历3级
            foreach ($v['secondlist'] as $k => &$t) {
                $t['thirdlist'] = db('area')->where('a_pid=' . $t['a_id'])->select();
                //遍历4级

                foreach ($t['thirdlist'] as $kk1 => $vv1) {
                    array_push($b, $vv1['a_name'] . $vv1['a_engname']);
                }
                foreach ($t['thirdlist'] as $k => &$f) {
                    $f['for'] = db('area')->where('a_pid=' . $f['a_id'])->select();

                    foreach ($f['for'] as $kk => $vv) {
                        array_push($a, $vv['a_name'] . $vv['a_engname']);
                    }
                }
            }
        }

        if (empty($a)) {

            $this->assign('depart', $b);

        } else {
            $this->assign('depart', $a);
        }
        $ship = db('area')->where("a_id='$ses'")->find();
        $sh = $ship['a_end_addr'];
        $this->assign('shi', $sh);
        return $this->fetch();


    }


    //提交收货表单详情
    public function ctrls()
    {
        $ses = session('a_companyid');
        $sess = session('a_receiveid');
        $data = input('post.');
        $data['r_owe_no'] = $data['r_owe_pay'];
        $data['r_owe_yes'] = 0;
        $data['r_return_no'] = $data['r_return_pay'];
        $data['r_return_yes'] = 0;


        $vername = $data['r_receiver'];
        $veriphon = $data['r_receiver_phone'];
        $intwhere = [
            "name" => $vername,
            "iphone" => $veriphon
        ];
        $veri = db('intelligent')->where($intwhere)->count('id');
        $aiinq = [
            'name' => $data['r_receiver'],
            'iphone' => $data['r_receiver_phone'],
            'companyid' => $ses
        ];
        if ($veri == 0) {
            db('intelligent')->insert($aiinq);
        } else {
            $veri = db('intelligent')->where($intwhere)->find();
            $veri['count'] = $veri['count'] + 1;
            $gai = [
                "count" => $veri['count']
            ];
            db('intelligent')->where($intwhere)->update($gai);
        }

        $verfname = $data['r_sender'];
        $verfiphon = $data['r_sender_phone'];
        $intwheres = [
            "name" => $verfname,
            "iphone" => $verfiphon
        ];
        $verif = db('intelligent')->where($intwheres)->count('id');
        $aiinqu = [
            'name' => $data['r_sender'],
            'iphone' => $data['r_sender_phone'],
            'companyid' => $ses,
            'cardid' => $data['r_card_no']
        ];
        if ($verif == 0) {
            db('intelligent')->insert($aiinqu);
        } else {
            $verif = db('intelligent')->where($intwheres)->find('id');
            $verif['count'] = $verif['count'] + 1;
            $gai = [
                "count" => $veri['count']
            ];
            db('intelligent')->where($intwheres)->update($gai);
        }


        $data['r_isclose'] = 1;
        if (empty($data['r_isback'])) {
            $data['r_isback'] = 2;
        }
        if (empty($data['r_isfollow'])) {
            $data['r_isfollow'] = 2;
        }
        if (empty($data['r_ismonsett'])) {
            $data['r_ismonsett'] = 2;
        }
        $aree = $data['r_area_id'];
        $idd = $data['idd'];
        $wheree = [
            'a_company_id' => ['=', session('a_companyid')],
            'a_name' => ['=', $aree]
        ];
        $are = db('area')->where($wheree)->find();//查询线路id

        $arerou = $are['a_engname'];
//        $data['r_area_id'] = $are['a_id'];


        $data['r_area_id'] = $idd;
        if ($data['r_area_id'] == 0) {
            $data['r_area_id'] = $are['a_id'];
        }
        $data['r_autosubmit'] =$are['a_autosubmit'];
        unset($data['idd']);
        //票号拼接 年月日
        $str = date('Y');
        $strm = date('m');
        $strd = date('d');
        $str = substr($str, -2);
        $strr = $str . $strm . $strd;


        //开始拼接货号
        $itewhere = [
            'i_companyid' => $ses,
            'i_areaid' => $sess
        ];
        $item = db('item')->where($itewhere)->find();
        if ($item == '') {
            $itdata = [
                'i_companyid' => $ses,
                'i_areaid' => $sess,
                'i_time' => $strr,
                'i_num' => 1
            ];
            db('item')->insert($itdata);

        } else {
            if ($strr != $item['i_time']) {
                $reset = [
                    'i_time' => $strr,
                    'i_num' => 0
                ];
                db('item')->where($itewhere)->update($reset);
                $item = db('item')->where($itewhere)->find();
                $itenum = $item['i_num'] + 1;
                $iteupdate = [
                    'i_num' => $itenum
                ];
                db('item')->where($itewhere)->update($iteupdate);
            } else {
                $item = db('item')->where($itewhere)->find();
                $itenum = $item['i_num'] + 1;
                $iteupdate = [
                    'i_num' => $itenum
                ];
                db('item')->where($itewhere)->update($iteupdate);
            }


        }
        $itemnu = db('item')->where($itewhere)->find();
        $iteshu = str_pad($itemnu['i_num'], 3, '0', STR_PAD_LEFT);
        $dbite = db('area')->where("a_id='$sess'")->find();
        $faengname = $dbite['a_engname'];
        $data['r_goods_no'] = $arerou . $strr . '-' . $faengname . $iteshu . '-' . $data['r_goods_no'];
        $jsoitem = $data['r_goods_no'];
        //货号拼接结束

        //开始拼接票号
        $tickwhere = [
            't_companyid' => $ses,
        ];
        $item = db('ticket')->where($tickwhere)->find();
        $itema = db('company')->where("c_id='$ses'")->find();

        if ($item == '') {
            $itdata = [
                't_companyid' => $ses,
//                'i_areaid' => $sess,
                't_time' => $strr,
                't_num' => 1
            ];
            db('ticket')->insert($itdata);

        } else {
            if ($strr != $item['t_time']) {
                $reset = [
                    't_time' => $strr,
                    't_num' => 0
                ];
                db('ticket')->where($tickwhere)->update($reset);
                $item = db('ticket')->where($tickwhere)->find();
                $itenum = $item['t_num'] + 1;
                $iteupdate = [
                    't_num' => $itenum
                ];
                db('ticket')->where($tickwhere)->update($iteupdate);
            } else {
                $item = db('ticket')->where($tickwhere)->find();
                $itenum = $item['t_num'] + 1;
                $iteupdate = [
                    't_num' => $itenum
                ];
                db('ticket')->where($tickwhere)->update($iteupdate);
            }
        }
        $itemnu = db('ticket')->where($tickwhere)->find();
        $iteshu = str_pad($itemnu['t_num'], 4, '0', STR_PAD_LEFT);////4改成5
//        $dbite = db('area')->where("a_id='$sess'")->find();
        $faengname = $dbite['a_engname'];
        $itema['c_abname'] = strtoupper($itema['c_abname']);
        if (config('prefix')==$ses){
            $data['r_ticket_no'] = $strr . '3'.$iteshu;///拼接的3去掉
        }else{
            $data['r_ticket_no'] = $itema['c_abname'] . $strr . $iteshu;
        }

        //票号拼接结束
        $jsoticket = $data['r_ticket_no'];


        $data['r_a_rid'] = $sess;
        $data['r_company_id'] = $ses;
        $data['r_createtime'] = time();
        if ($data['r_transform_pay']!=''||$data['r_give_money']!=''){
            $data['r_audit']=1;
        }
        if ($data['r_transform_pay']==''&&$data['r_give_money']==''){
            $data['r_audit']=0;
        }

        $mol = new Receivem();
        $mol->inse($data);


        $data['r_companyid'] = $ses;
        $data['type'] = '收货';
        $mode = new Track();
        $re = $mode->billbing($data);

        //二维码开始生成页
        header("Content-Type: text/html;charset=utf-8");
        //引入二维码生成插件
        vendor("phpqrcode.phpqrcode");
        // 生成的二维码所在目录+文件名
        $path = "./static/uploads/QRcode/";//生成的二维码所在目录
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $time = time() . '.png';//生成的二维码文件名
        $fileName = $path . $time;//1.拼装生成的二维码文件路径
        $dataes = config('base_url') . "/index.php/wap/index/id/$jsoticket/comid/$ses";//2.生成二维码的数据(扫码显示该数据)
        $level = 'L';  //3.纠错级别：L、M、Q、H
        $size = 4;    //4.点的大小：1到10,用于手机端4就可以了
        ob_end_clean();//清空缓冲区
        \QRcode::png($dataes, $fileName, $level, $size);//生成二维码
//        $this->assign("src","/static/uploads/QRcode/".$time);
        $json = [
            'itam' => $jsoitem,
            'tichet' => $jsoticket,
            'img' => "/static/uploads/QRcode/" . $time
        ];
        return json_encode($json);


    }

    //手动
    public function ctrlss()
    {
        $ses = session('a_companyid');
        $sess = session('a_receiveid');
        $data = input('post.');
        $data['r_owe_no'] = $data['r_owe_pay'];
        $data['r_owe_yes'] = 0;
        $data['r_return_no'] = $data['r_return_pay'];
        $data['r_return_yes'] = 0;


        $vername = $data['r_receiver'];
        $veriphon = $data['r_receiver_phone'];
        $intwhere = [
            "name" => $vername,
            "iphone" => $veriphon
        ];
        $veri = db('intelligent')->where($intwhere)->count('id');
        $aiinq = [
            'name' => $data['r_receiver'],
            'iphone' => $data['r_receiver_phone'],
            'companyid' => $ses
        ];
        if ($veri == 0) {
            db('intelligent')->insert($aiinq);
        } else {
            $veri = db('intelligent')->where($intwhere)->find();
            $veri['count'] = $veri['count'] + 1;
            $gai = [
                "count" => $veri['count']
            ];
            db('intelligent')->where($intwhere)->update($gai);
        }

        $verfname = $data['r_sender'];
        $verfiphon = $data['r_sender_phone'];
        $intwheres = [
            "name" => $verfname,
            "iphone" => $verfiphon
        ];
        $verif = db('intelligent')->where($intwheres)->count('id');
        $aiinqu = [
            'name' => $data['r_sender'],
            'iphone' => $data['r_sender_phone'],
            'companyid' => $ses,
            'cardid' => $data['r_card_no']
        ];
        if ($verif == 0) {
            db('intelligent')->insert($aiinqu);
        } else {
            $verif = db('intelligent')->where($intwheres)->find('id');
            $verif['count'] = $verif['count'] + 1;
            $gai = [
                "count" => $veri['count']
            ];
            db('intelligent')->where($intwheres)->update($gai);
        }


        $data['r_isclose'] = 1;
        if (empty($data['r_isback'])) {
            $data['r_isback'] = 2;
        }
        if (empty($data['r_isfollow'])) {
            $data['r_isfollow'] = 2;
        }
        if (empty($data['r_ismonsett'])) {
            $data['r_ismonsett'] = 2;
        }
        $aree = $data['r_area_id'];
        $idd = $data['idd'];
        $wheree = [
//            'a_id'=>['=',$idd],
            'a_company_id' => ['=', session('a_companyid')],
            'a_name' => ['=', $aree]
        ];
        $are = db('area')->where($wheree)->find();//查询线路id
        $arerou = $are['a_engname'];
//        $data['r_area_id'] = $are['a_id'];


        $data['r_area_id'] = $idd;
        if ($data['r_area_id'] == 0) {
            $data['r_area_id'] = $are['a_id'];
        }
        $data['r_autosubmit'] =$are['a_autosubmit'];
        unset($data['idd']);


        //票号拼接 年月日
        $str = date('Y');
        $strm = date('m');
        $strd = date('d');
        $str = substr($str, -2);
        $strr = $str . $strm . $strd;

        $dbite = db('area')->where("a_id='$sess'")->find();
        $faengname = $dbite['a_engname'];


        $data['r_goods_no'] = $arerou.$strr.'-'.$faengname.$data['p-shou'] .'-'. $data['r_goods_no'];
        $nu = $data['r_goods_no'];
        $number = db('receive')->where("r_goods_no='$nu'")->count();
        if ($number != 0) {
            return 4;
        } else {

            unset($data['p-shou']);
            $jsoitem = $data['r_goods_no'];

            $jsoticket = $data['r_ticket_no'];
            $numbers = db('receive')->where("r_ticket_no='$jsoticket'")->count();
            if ($numbers != 0) {
                return 5;
            } else {
                $data['r_a_rid'] = $sess;
                $data['r_company_id'] = $ses;
                $data['r_createtime'] = time();
                if ($data['r_transform_pay']!=''||$data['r_give_money']!=''){
                    $data['r_audit']=1;
                }
                if ($data['r_transform_pay']==''&&$data['r_give_money']==''){
                    $data['r_audit']=0;
                }
                $mol = new Receivem();
                $mol->inse($data);
                $data['r_companyid'] = $ses;
                $data['type'] = '收货';
                $mode = new Track();
                $re = $mode->billbing($data);

                //二维码开始生成页
                header("Content-Type: text/html;charset=utf-8");
                //引入二维码生成插件
                vendor("phpqrcode.phpqrcode");
                // 生成的二维码所在目录+文件名
                $path = "./static/uploads/QRcode/";//生成的二维码所在目录
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $time = time() . '.png';//生成的二维码文件名
                $fileName = $path . $time;//1.拼装生成的二维码文件路径
                $dataes = config('base_url')."/index.php/Wap/index/id/$jsoticket/comid/$ses";//2.生成二维码的数据(扫码显示该数据)
                $level = 'L';  //3.纠错级别：L、M、Q、H
                $size = 4;    //4.点的大小：1到10,用于手机端4就可以了
                ob_end_clean();//清空缓冲区
                \QRcode::png($dataes, $fileName, $level, $size);//生成二维码
//        $this->assign("src","/static/uploads/QRcode/".$time);
                $json = [
                    'itam' => $jsoitem,
                    'tichet' => $jsoticket,
                    'img' => "/static/uploads/QRcode/" . $time
                ];
                return json_encode($json);
            }


        }


    }


    public function layer_add()
    {
        return $this->fetch();
    }

    public function layer_do_add()
    {
        $companyid = session('a_companyid');
        $where = [
            'a_company_id' => $companyid,
            'a_pid' => 0
        ];

        $area = db('area')->where($where)->order('a_id desc')->select();
        foreach ($area as $key => &$v) {
            $v['secondlist'] = db('area')->where('a_pid=' . $v['a_id'])->select();
            foreach ($v['secondlist'] as $k => &$t) {
                $t['thirdlist'] = db('area')->where('a_pid=' . $t['a_id'])->select();
                foreach ($t['thirdlist'] as $ke => &$f) {
                    $f['forlist'] = db('area')->where('a_pid=' . $f['a_id'])->select();
                }
            }
        }
        return json_encode($area);
    }

//智能查询
    public function zhihui()
    {
        $ses = session('a_companyid');
        $ad = input('aa');
        if ($ad == '') {
            return json_encode('');
        } else {
            $where = [
                'name' => ['like', "%" . $ad . "%"],
                'companyid' => $ses
            ];
            $db = db('intelligent')->where($where)->order('count desc')->limit(5)->select();
            if ($db == '') {
                return json_encode('');
            } else {
                return json_encode($db);
            }

        }

    }

    //地址输入中英自动改变
    public function exchange()
    {
        $data = input('data');
        if (preg_match('/^[a-z]+$/', $data)) {
            $data = strtoupper($data);
        }

        $len = preg_match('/^[\x{4e00}-\x{9fa5}]/u', $data);

        $company = session('a_companyid');
        $a = [];
        $b = [];
        $c = [];
        $d = [];
        $where = [
            'a_company_id' => $company,
            'a_pid' => ['=', 0]
        ];
        $area = db('area')->where($where)->select();
        foreach ($area as $key => &$v) {
            $v['secondlist'] = db('area')->where('a_pid=' . $v['a_id'])->select();
            //遍历3级
            foreach ($v['secondlist'] as $k => &$t) {
                $t['thirdlist'] = db('area')->where('a_pid=' . $t['a_id'])->select();
                //遍历4级

                foreach ($t['thirdlist'] as $kk1 => $vv1) {
                    array_push($c, $vv1['a_engname']);
                    array_push($d, $vv1['a_name']);
                }
                foreach ($t['thirdlist'] as $k => &$f) {
                    $f['for'] = db('area')->where('a_pid=' . $f['a_id'])->select();

                    foreach ($f['for'] as $kk => $vv) {
                        array_push($a, $vv['a_name']);
                        array_push($b, $vv['a_engname']);
                    }
                }
            }
        }



        $nav = [
            'a_company_id' => $company,
//            'a_pid' => ['=', 0],
            'a_name'=>$data,

        ];
        $navs = [
            'a_company_id' => $company,
//            'a_pid' => ['=', 0],
            'a_engname'=>$data,

        ];

        if ($len) {
            //中文
            if (!empty($data)) {

                if (!empty($a)) {
                    if (in_array($data, $a)) {
//                        $info = db('area')->where("a_name='" . "$data'")->find();
                        $info = db('area')->where($nav)->find();
                        return json_encode($info);
                    }
                } else {
                    if (in_array($data, $d)) {
                        $info = db('area')->where($nav)->find();
                        return json_encode($info);
                    }
                }


            }
        } else {
            //英文
            if (!empty($data)) {
                if (!empty($b)) {
                    if (in_array($data, $b)) {

                        $info = db('area')->where($navs)->find();
                        return json_encode($info);
                    }
                } else {
                    if (in_array($data, $c)) {
                        $info = db('area')->where($navs)->find();
                        return json_encode($info);
                    }
                }

            }
        }
    }

    //地址点击中英自动改变
    public function exchange1()
    {
        $data = input('data');
        $len = preg_match('/^[\x{4e00}-\x{9fa5}]/u', $data);
        if ($len) {
            if (!empty($data)) {
                $info = db('area')->where("a_id='$data'")->find();
                return json_encode($info);
            }
        } else {
            if (!empty($data)) {
                $info = db('area')->where("a_id='$data'")->find();
                return json_encode($info);
            }
        }
    }


    public function base()
    {

        return $this->fetch();
    }

//模拟url传输
    public function url(){

        $data = input('id');
        //设置一个浏览器agent的header
        $header = ['Content-Type: application/json','x-token:7043227a167b4968a177b60378bfb1be'];
        // 防止CURL执行超时
        set_time_limit(0);


        // 请求地址
        $url = 'http://114.55.5.102:8090/api/aa/member?no='.$data;

        // 初始化一个新会话
        $ch = curl_init();

        // 设置要求请的url
        curl_setopt($ch, CURLOPT_URL, $url);

        // 是否验证SSL证书
        // 一般不验证 ( 默认为验证 需设置fasle关闭 )
        // 如果设置false报错 尝试改为 0
        // 某些CURL版本不只true和fasle两种状态 可能是0,1,2...等
        // 如果选择验证证书 将参数设置为ture或1
        // 然后在使用CURLOPT_CAPATH和CURLOPT_CAINFO设置证书信息
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        // 验证证书中域名是否与当前域名匹配 和上面参数配合使用
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        // 是否将数据已返回值形式返回
        // 1 返回数据
        // 0 直接输出数据
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // 执行CURL请求
        $output = curl_exec($ch);

        // 关闭CURL资源
        curl_close($ch);

        // 输出返回信息
        return json_encode($output);

    }


    //参送参转费审核页面
    public function reve()
    {
        $f_v = input("field_value");
        $f = input('field_name');
        $p_m = input('pick_type');
        $olds_t = input('starttime');
        $olde_t = input('endtime');
        $r_u = input('receive_unit');
        $a_u = input('arrive_unit');
        $s_t = strtotime($olds_t);
        $e_t = strtotime($olde_t);
        $payed = input('ispayed');
        $r_i = input('r_include');
        $a_i = input('a_include');
        $rid = input('rid');
        $aid = input('aid');
        $this->assign('aid', $aid);
        $data['aid'] = $aid;
        $this->assign('r_include', $r_i);
        $this->assign('a_include', $a_i);
        $this->assign('arrive_unit', $a_u);
        $this->assign('ispayed', $payed);
        $this->assign('pick_type', $p_m);
        $this->assign('field_name', $f);
        $this->assign("field_value", $f_v);
        $data['r_include'] = $r_i;
        $data['a_include'] = $a_i;
        $data['field_value'] = $f_v;
        $data['field_name'] = $f;
        $data['pick_type'] = $p_m;
        $data['starttime'] = $s_t;
        $data['endtime'] = $e_t;
        $data['ispayed'] = $payed;
        $data['receive_unit'] = $r_u;
        $data['arrive_unit'] = $a_u;
        $data['rid'] = $rid;
        $receive2 = new Receivem();
        $list = $receive2->getSealList($data);
        $seal_list = $list['list'];
        if ($rid == '') {
            $rid = $list['rid'];
        }
        $this->assign('rid', $rid);
        if ($r_u == '') {
            $r_u = $list['r_u'];
        }
        $this->assign('receive_unit', $r_u);
        if ($olds_t == '') {
            $olds_t = $list['starttime'];
        }
        if ($olde_t == '') {
            $olde_t = $list['endtime'];
        }
        $this->assign('starttime', $olds_t);
        $this->assign('endtime', $olde_t);
        $this->assign('seal_list', $seal_list);
        return $this->fetch();
    }
    //修改费用
    public function updateMoney()
    {
        $data=[
            'r_id'=>input('id'),
            'r_give_money'=>input('s'),
            'r_transform_pay'=>input('z')
        ];
        $db=new Receivem();
        $list=$db->updateMoney($data);
        return json_encode($list);
    }
    //确定审核
    public function saveState()
    {
        $id = input('id');
        $db = new Receivem();
        $info = $db->saveState($id);
        return json_encode([$id,$info]);
    }

    //发货站点
    public function layer_do_ad()
    {
        $type = input('type');
        $receive2 = new Receivem();
        $area = $receive2->getAreaList($type);
        return json_encode($area);
    }
    //收货站点
    public function r_area()
    {
        $type = input('type');
        $receive2 = new Receivem();
        $area = $receive2->getAreaList2($type);
        return json_encode($area);
    }

    //封单提交列表----修改
    public function receive_edit()
    {
        $id = input('r_id');
        $data = input('post.');
        $receive2 = new Receivem();
        $log = new Track();
        if (empty($data)) {
            $oldinfo = $receive2->r_edit($id);
            $today = date('Y-m-d', time());
            $this->assign('today', $today);
            $this->assign('info', $oldinfo);
            //地区埠
            $a = [];
            $b = [];
            $where = [
                'a_company_id' => session('a_companyid'),
                'a_pid' => ['=', 0]
            ];
            $area = db('area')->where($where)->select();
            foreach ($area as $key => &$v) {
                $v['secondlist'] = db('area')->where('a_pid=' . $v['a_id'])->select();
                //遍历3级
                foreach ($v['secondlist'] as $k => &$t) {
                    $t['thirdlist'] = db('area')->where('a_pid=' . $t['a_id'])->select();
                    //遍历4级

                    foreach ($t['thirdlist'] as $kk1 => $vv1) {
                        array_push($b, $vv1['a_name'] . $vv1['a_engname']);
                    }
                    foreach ($t['thirdlist'] as $k => &$f) {
                        $f['for'] = db('area')->where('a_pid=' . $f['a_id'])->select();

                        foreach ($f['for'] as $kk => $vv) {
                            array_push($a, $vv['a_name'] . $vv['a_engname']);
                        }
                    }
                }
            }

            if (empty($a)) {

                $this->assign('depart', $b);

            } else {
                $this->assign('depart', $a);
            }
            return $this->fetch();
        } else {
            $data['r_audit']=0;
            $newinfo = $receive2->do_edit($data);
            if ($newinfo['code'] == 1) {
                $logdata['r_goods_no'] = $data['r_goods_no'];
                $logdata['r_ticket_no'] = $data['r_ticket_no'];
                $logdata['r_operator'] = session('a_username');
                $logdata['r_company_id'] = session('a_companyid');
                $logdata['r_createtime'] = time();
                $logdata['type'] = "修改";
                $log->billbing($logdata);
            }
            return json_encode($newinfo);
        }
    }

    //查询封单之前的合计
    public function seal_sum()
    {
        //获取数据
        $f_v = input("field_value");
        $f = input('field_name');
        $p_m = input('pick_type');
        $olds_t = input('starttime');
        $olde_t = input('endtime');
        $alltype = input('alltype');
        $s_t = strtotime($olds_t);
        $e_t = strtotime($olde_t);
        $payed = input('ispayed');
        $r_u = input('receive_unit');
        $a_u = input('arrive_unit');
        $r_i = input('r_include');
        $a_i = input('a_include');
        $rid = input('rid');
        $aid = input('aid');
        $data['rid'] = $rid;
        $data['aid'] = $aid;
        //合成数组传入model
        $data['r_include'] = $r_i;
        $data['a_include'] = $a_i;
        $data['field_value'] = $f_v;
        $data['field_name'] = $f;
        $data['pick_type'] = $p_m;
        $data['starttime'] = $s_t;
        $data['endtime'] = $e_t;
        $data['ispayed'] = $payed;
        $data['alltype'] = $alltype;
        $data['receive_unit'] = $r_u;
        $data['arrive_unit'] = $a_u;
        $receive2 = new Receivem();
        $arr = $receive2->seal1_sum($data);
        return json($arr);
    }


}




